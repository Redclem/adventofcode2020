#include "Day24.h"
#include "Data.h"


int main()
{
    Day24 day(data);
    day.run();
    return 0;
}
