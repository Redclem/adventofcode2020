#include "Day13.h"

Day13::Day13(const char * data)
{
    char * next;
    m_earliest_depart = strtoul(data, &next, 10);
    data = next;
    if(*data == '\n') data++;

    uint32_t position(0);

    while(*data)
    {
        if(*data == 'x')
            data++;
        else
        {
            m_lines.emplace_back(strtoul(data, &next, 10), position);
            data = next;
        }

        if(*data == ',') data++;

        position++;
    }

    //ctor
}

Day13::~Day13()
{
    //dtor
}

void Day13::run()
{
    uint32_t best_line_id(0);
    uint32_t best_wait(UINT32_MAX);

    for(LineInfo & line : m_lines)
    {
        uint32_t wait(0);
        if(m_earliest_depart % line.id)
        {
            wait = line.id - (m_earliest_depart % line.id);
        }
        if(wait < best_wait)
        {
            best_wait = wait;
            best_line_id = line.id;
        }
    }

    std::cout << "Earliest bus line * wait : " << best_line_id * best_wait << '\n';

    int64_t a(1);
    int64_t res(0);


    for(LineInfo & line : m_lines)
    {
        res = solve(a, line.id, res + line.position) + res;
        a *= line.id;
    }

    std::cout << "Earliest timestamp such that all of the listed bus IDs depart at offsets matching their positions in the list : " <<  res << '\n';
}

int64_t Day13::solve(int64_t a, int64_t b, int64_t d)
{
    int64_t u = Math::modular_inverse<int64_t>(a, b);

    int64_t res = a * (u * (-d) % b);

    return (res < 0 ? res + a * b : res);
}

