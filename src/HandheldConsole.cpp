#include "HandheldConsole.h"

HandheldConsole::HandheldConsole() : m_ip(0), m_acc(0)
{
    //ctor
}

HandheldConsole::~HandheldConsole()
{
    //dtor
}

void HandheldConsole::load_instructions(const char* data)
{
    while(*data != 0)
    {
        Operation operation;
        IF_OP_MATCH(nop);
        ELSE_IF_OP_MATCH(acc);
        ELSE_IF_OP_MATCH(jmp);
        else
        {
            std::cout << "Unknown instruction";
            throw std::runtime_error("Unknown instruction");
        }

        data += 3;

        char * next;
        arg_t arg = static_cast<arg_t>(strtol(data, &next, 10));

        data = next;

        m_instructions.push_back({operation, arg});

        if(*data == '\n')
            data++;
    }
}

void HandheldConsole::step()
{
    Instruction instr = m_instructions[m_ip];
    switch(instr.op)
    {
    case Operation::acc:
        m_acc += instr.argument;
        [[clang::fallthrough]];
    case Operation::nop:
        m_ip++;
        break;
    case Operation::jmp:
        m_ip = m_ip + static_cast<size_t>(instr.argument);
        break;
    }
}

std::vector<bool> HandheldConsole::run_no_rep()
{
    std::vector<bool> m_instr_exec(m_instructions.size(), false);
    while(!m_instr_exec[m_ip])
    {
        m_instr_exec[m_ip] = true;
        step();
    }
    return m_instr_exec;
}

bool HandheldConsole::check_exec_first_nonex()
{
    std::vector<bool> m_instr_exec(m_instructions.size(), false);
    while(!m_instr_exec[m_ip])
    {
        if(m_ip == m_instructions.size())
            return true;
        else if(m_ip > m_instructions.size())
            return false;

        m_instr_exec[m_ip] = true;
        step();
    }
    return false;
}


