#include "Day19.h"

Day19::Day19(const char * data)
{
    while(*data != '\n')
    {
        char * next;
        size_t line_index(strtoull(data, &next, 10));


        data = next;
        if(*data == ':')
            data++;

        while(isspace(*data))
            data++;

        if(*data == '"')
        {
            data++;
            m_simple_rules[line_index] = *data;
        }
        else
        {
            auto & cur_rule = m_composite_rules[line_index];
            cur_rule = std::vector<std::vector<size_t>>(1);


            do
            {

                while(*data == ' ')
                    data++;

                size_t rule = strtoull(data, &next, 10);


                cur_rule.back().push_back(rule);

                data = next;
                while(*data == ' ')
                {
                    data++;
                }
                if(*data == '|')
                {
                    data++;
                    cur_rule.back().shrink_to_fit();
                    cur_rule.emplace_back();
                    while(*data == ' ')
                    {
                        data++;
                    }
                }
            }
            while(*data != '\n');

            cur_rule.shrink_to_fit();
        }

        data = strchr(data, '\n') + 1;
    }

    data++;

    while(*data)
    {
        const char * line_end = strchr(data, '\n');
        if(!line_end)
        {
            line_end = strchr(data, 0);
        }

        m_messages.emplace_back(data, line_end);

        data = line_end;

        if(*data == '\n')
            data++;
    }
}

Day19::~Day19()
{
    //dtor
}

void Day19::run()
{
    size_t sum(0);

    for(const std::string & msg : m_messages)
    {
        const char * c_str = msg.c_str();
        if(msg_match_rule(0, c_str) && *c_str == 0)
            sum++;
    }

    std::cout << m_messages.size() << '\n';
    std::cout << sum << " messages match rule 0\n";

    sum = 0;

    for(const std::string & msg : m_messages)
    {
        const char * c_str = msg.c_str();
        if(msg_match_new_rule(c_str))
        {
            //std::cout << msg << '\n';
            sum++;
        }
    }

    std::cout << sum << " messages match rule 0 with modified rules\n";
}

bool Day19::msg_match_rule(size_t rule, const char*& msg)
{

    if(m_simple_rules.count(rule))
    {
        return *(msg++) == m_simple_rules[rule];
    }
    else
    {
        std::vector<std::vector<size_t>> & rule_ref = m_composite_rules[rule];

        for(size_t subrule(0); subrule != rule_ref.size(); subrule++)
        {
            std::vector<size_t> subrule_ref = rule_ref[subrule];

            bool match(true);

            const char * cpy = msg;

            for(size_t subrule_id : subrule_ref)
            {
                if(!msg_match_rule(subrule_id, cpy))
                {
                    match = false;
                    break;
                }
            }

            if(match)
            {
                msg = cpy;
                return true;
            }
        }

        return false;
    }
}

bool Day19::msg_match_new_rule(const char*& msg)
{
    size_t A(0);
    const char * cpy(msg);
    while(msg_match_rule(42, cpy))
    {
        A++;
    }

    if(A < 2)
        return false;

    for(size_t a(2); a <= A; a++)
    {
        cpy = msg;

        for(size_t run(0); run != a; run++)
            msg_match_rule(42, cpy);


        if(!msg_match_rule(31, cpy))
            continue;

        bool fail(false);

        size_t d(1);

        while(isalpha(*cpy))
        {
            if(!msg_match_rule(31, cpy))
            {
                fail = true;
                break;
            }
            d++;
        }

        if(!fail)
        {
            std::cout << a << ' ' << d << '\n';

            if(d >= a)
            {
                std::cout << "d >= a, invalid\n";
            }
            else
                return true;
        }
    }
    return false;
}




