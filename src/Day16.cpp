#include "Day16.h"

Day16::Day16(const char * data)
{
    while(*data != '\n')
    {
        Field new_field;
        const char * nameend(strchr(data, ':'));
        new_field.name.assign(data, nameend);
        data = nameend + 1;

        char * next;
        new_field.min0 = strtoull(data, &next, 10);
        data = next + 1;
        new_field.max0 = strtoull(data, &next, 10);
        data = next + 3;

        new_field.min1 = strtoull(data, &next, 10);
        data = next + 1;
        new_field.max1 = strtoull(data, &next, 10);
        data = next;

        m_fields.push_back(new_field);

        if(*data == '\n') data++;
    }

    m_fields.shrink_to_fit();

    data++;

    if(strncmp(data, "your ticket:", 12))
    {
        std::cout << "Expected \"your ticket:\"\n";
        return;
    }
    else
    {
        data += 13;
        while(*data != '\n')
        {
            char * next;
            m_my_ticket.push_back(strtoull(data, &next, 10));
            data = next;
            if(*data == ',') data++;
        }

        m_my_ticket.shrink_to_fit();
    }

    data += 2;

    if(strncmp(data, "nearby tickets:", 15))
    {
        std::cout << "Expected \"nearby tickets:\"\n";
        return;
    }
    else
    {
        data += 16;
        while(*data)
        {
            m_tickets.emplace_back();
            while(*data != '\n' && *data)
            {
                char * next;
                m_tickets.back().push_back(strtoull(data, &next, 10));
                data = next;

                if(*data == ',')
                    data++;
            }

            m_tickets.back().shrink_to_fit();

            if(*data == '\n')
                data++;
        }
        m_tickets.shrink_to_fit();
    }

    //ctor
}

bool Day16::check_value_valid(uint64_t val)
{
    for(Field & fld : m_fields)
    {
        if((val >= fld.min0 && val <= fld.max0) || (val >= fld.min1 && val <= fld.max1))
            return true;
    }
    return false;
}

Day16::~Day16()
{
    //dtor
}

void Day16::run()
{
    uint64_t ticket_scan_error_rate(0);
    std::vector<bool> valid_tickets(m_tickets.size(), true);

    for(size_t ticket(0); ticket != m_tickets.size(); ticket++)
        for(uint64_t value : m_tickets[ticket])
        {
            if(!check_value_valid(value))
            {
                ticket_scan_error_rate += value;
                valid_tickets[ticket] = false;
            }
        }

    std::cout << "ticket scanning error rate : " << ticket_scan_error_rate << '\n';

    std::vector<size_t> field_map(m_fields.size(), size_t(~0));

    std::vector<std::vector<bool>> can_be_fields(m_my_ticket.size(), std::vector<bool>(m_fields.size(), true));

    for(size_t ticket_field(0); ticket_field != m_my_ticket.size(); ticket_field++)
    {
        for(size_t ticket(0); ticket != m_tickets.size(); ticket++)
        {
            if(!valid_tickets[ticket])
                continue;

            for(size_t field(0); field != m_fields.size(); field++)
            {
                if(can_be_fields[ticket_field][field])
                {
                    if(!check_value_valid_fld(m_tickets[ticket][ticket_field], field))
                    {
                        can_be_fields[ticket_field][field] = false;
                    }
                }
            }
        }
    }

    size_t assoc_fields(0);

    std::vector<bool> ticket_field_associated(m_my_ticket.size(), false);

    while(assoc_fields != m_fields.size())
    {
        for(size_t ticket_field(0); ticket_field != m_my_ticket.size(); ticket_field++)
        {
            if(ticket_field_associated[ticket_field]) continue;

            size_t avail_fields(0);
            size_t last_fld(0);
            for(size_t dst_fld(0);dst_fld != m_fields.size();dst_fld++)
            {
                if(can_be_fields[ticket_field][dst_fld])
                {
                    avail_fields++;
                    last_fld = dst_fld;
                }
            }


            if(avail_fields == 0)
            {
                std::cout << "Error : ticket field cannot be any field\n";
                return;
            }

            if(avail_fields == 1)
            {
                field_map[last_fld] = ticket_field;
                for(size_t tic_fld(0);tic_fld != m_my_ticket.size();tic_fld++)
                {
                    can_be_fields[tic_fld][last_fld] = false;
                }
                ticket_field_associated[ticket_field] = true;

                assoc_fields++;
            }
        }
    }

    uint64_t prod(1);

    for(size_t field(0); field != m_fields.size();field++)
    {
        if(strncmp(m_fields[field].name.c_str(), "departure", 9) == 0)
        {
            prod *= m_my_ticket[field_map[field]];
        }
    }

    std::cout << "Product of the 6 \"departure\" fields : " << prod << '\n';
}
