#include "Ferry.h"

Ferry::Ferry() : m_direction(Direction::EAST), m_x(0), m_y(0), m_way_x(10), m_way_y(1)
{
    //ctor
}

Ferry::~Ferry()
{
    //dtor
}

void Ferry::rotate(degree_t deg, bool right)
{
    if(right)
        reinterpret_cast<uint32_t&>(m_direction) += deg / 90;
    else
        reinterpret_cast<uint32_t&>(m_direction) -= deg / 90;
    reinterpret_cast<uint32_t&>(m_direction) %= 4;
}

void Ferry::rotate_way(degree_t deg, bool right)
{
    if(!right)
        deg = 360 - deg;

    position_t old_y = m_way_y;

    switch(deg)
    {
    case 90:
        m_way_y = -m_way_x;
        m_way_x = old_y;
        break;
    case 180:
        m_way_x = -m_way_x;
        m_way_y = -m_way_y;
        break;
    case 270:
        m_way_y = m_way_x;
        m_way_x = -old_y;
        break;
    default:
        std::cout << "Wrong angle\n";
        throw std::runtime_error("Wrong angle");
    }
}

void Ferry::go_forward(distance_t dst)
{
    m_x += dist_directions[static_cast<uint32_t>(m_direction)][0] * static_cast<position_t>(dst);
    m_y += dist_directions[static_cast<uint32_t>(m_direction)][1] * static_cast<position_t>(dst);
}

void Ferry::go_forward_way(distance_t dst)
{
    m_x += m_way_x * static_cast<position_t>(dst);
    m_y += m_way_y * static_cast<position_t>(dst);
}

void Ferry::travel(distance_t dst, Direction dir)
{
    m_x += dist_directions[static_cast<uint32_t>(dir)][0] * static_cast<position_t>(dst);
    m_y += dist_directions[static_cast<uint32_t>(dir)][1] * static_cast<position_t>(dst);
}

Ferry::Direction Ferry::dir_from_char(char chr)
{
    switch(chr)
    {
    case 'N':
        return Direction::NORTH;
    case 'E':
        return Direction::EAST;
    case 'S':
        return Direction::SOUTH;
    case 'W':
        return Direction::WEST;
    default:
        std::cout << "Unknown direction\n";
        throw std::runtime_error("Unknown direction");
    }
}

void Ferry::exec_instr(char chr, value_t value)
{
    switch(chr)
    {
    case 'N':
    case 'E':
    case 'S':
    case 'W':
        travel(value, dir_from_char(chr));
        break;
    case 'R':
    case 'L':
        rotate(value, chr == 'R');
        break;
    case 'F':
        go_forward(value);
        break;
    default:
        std::cout << "Unknown instruction\n";
        throw std::runtime_error("Unknown instruction");
    }
}

void Ferry::exec_instr_way(char chr, value_t value)
{
    switch(chr)
    {
    case 'N':
    case 'E':
    case 'S':
    case 'W':
        travel_way(value, dir_from_char(chr));
        break;
    case 'R':
    case 'L':
        rotate_way(value, chr == 'R');
        break;
    case 'F':
        go_forward_way(value);
        break;
    default:
        std::cout << "Unknown instruction\n";
        throw std::runtime_error("Unknown instruction");
    }
}

void Ferry::reset()
{
    m_direction = Direction::EAST;
    m_x = 0;
    m_y = 0;
    m_way_x = 10;
    m_way_y = 1;
}

void Ferry::travel_way(distance_t dst, Direction dir)
{
    m_way_x += dist_directions[static_cast<uint32_t>(dir)][0] * static_cast<position_t>(dst);
    m_way_y += dist_directions[static_cast<uint32_t>(dir)][1] * static_cast<position_t>(dst);
}

