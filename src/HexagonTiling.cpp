#include "HexagonTiling.h"

void HexagonTiling::flip(const HexagonNav& tile)
{
    auto insr = m_flipped_tiles.insert(tile);
    if(insr.second == false)
    {
        m_flipped_tiles.erase(insr.first);
    }
}

void HexagonTiling::flip_art()
{
    std::map<HexagonNav, size_t> neighbor_count;

    constexpr int64_t directions[6][2] = {{2, 0}, {1, 1}, {-1, 1}, {-2, 0}, {-1, -1}, {1, -1}};

    for(const HexagonNav & flipped : m_flipped_tiles)
        for(size_t dir(0); dir != 6; dir++)
        {
            neighbor_count[ {flipped.x() + directions[dir][0], flipped.y() + directions[dir][1]}]++;
        }

    {
        auto neighbor_iter = neighbor_count.begin();

        auto flipped_iter = m_flipped_tiles.begin();

        while(flipped_iter != m_flipped_tiles.end())
        {
            while(neighbor_iter->first < *flipped_iter && neighbor_iter != neighbor_count.end())
                neighbor_iter++;

            if(neighbor_iter->first != *flipped_iter)
                flipped_iter = m_flipped_tiles.erase(flipped_iter);
            else
                flipped_iter++;
        }
    }

    {
        auto neighbor_iter = neighbor_count.begin();

        auto flipped_iter = m_flipped_tiles.begin();

        while(neighbor_iter != neighbor_count.end())
        {

            while(*flipped_iter < neighbor_iter->first && flipped_iter != m_flipped_tiles.end())
            {
                flipped_iter++;
            }

            if(neighbor_iter->second == 2)
            {
                flipped_iter = m_flipped_tiles.insert(flipped_iter, neighbor_iter->first);
                flipped_iter++;
            }
            else if(neighbor_iter->second > 2)
            {
                if(flipped_iter != m_flipped_tiles.end() && *flipped_iter == neighbor_iter->first)
                {
                    flipped_iter = m_flipped_tiles.erase(flipped_iter);
                }
            }

            neighbor_iter++;
        }
    }
}
