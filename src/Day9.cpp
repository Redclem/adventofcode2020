#include "Day9.h"

Day9::Day9(const char * data)
{
    while(*data)
    {
        char * next;
        m_numbers.push_back(strtoull(data, &next, 10));
        data = next;
        if(*data == '\n')
            data++;
    }
    //ctor
}

Day9::~Day9()
{
    //dtor
}

void Day9::run()
{
    constexpr size_t preamble_size(25);

    number_t first_invalid(0);

    for(size_t number(preamble_size); number != m_numbers.size(); number++)
    {
        size_t part0_limit(number - 1);

        bool sum_found(false);

        for(size_t part0(number - preamble_size); (part0 != part0_limit) && !sum_found; part0++)
        {
            if(m_numbers[part0] > m_numbers[number])
                continue;

            number_t complement = m_numbers[number] - m_numbers[part0];

            for(size_t part1(part0 + 1); part1 != number; part1++)
                if(m_numbers[part1] == complement)
                {
                    sum_found = true;
                    break;
                }
        }
        if(!sum_found)
        {
            std::cout << "First number that is not a sum of 2 of the 25 previous numbers : " << m_numbers[number] << '\n';
            first_invalid = m_numbers[number];
            break;
        }
    }

    size_t sum_begin(0);
    size_t sum_end(1);

    number_t sum(m_numbers[0]);

    while(sum_end != m_numbers.size() || sum <= first_invalid)
    {
        if(sum < first_invalid)
        {
            sum += m_numbers[sum_end];
            sum_end++;
        }
        else if (sum > first_invalid)
        {
            sum -= m_numbers[sum_begin];
            sum_begin++;
        }
        else
            break;
    }

    number_t sum_min(static_cast<number_t>(~0));
    number_t sum_max(0);

    for(size_t num(sum_begin); num != sum_end;num++)
    {
        if(m_numbers[num] < sum_min)
            sum_min = m_numbers[num];
        if(m_numbers[num] > sum_max)
            sum_max = m_numbers[num];
    }

    std::cout << "Encryption weakness : " << sum_min + sum_max << '\n';
}

