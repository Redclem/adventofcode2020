#include "Day1.h"

Day1::Day1(const char* data)
{
    char * next;
    while(*data != 0)
    {
        uint64_t entry = strtoull(data, &next, 10);
        m_entries.push_back(entry);
        data = next;
        if(*data == '\n') data++;
    }
}

void Day1::run()
{
    bool cont(true);
    for(size_t entry0(0); entry0 != m_entries.size() && cont; entry0++)
        for(size_t entry1(entry0 + 1); entry1 != m_entries.size() && cont; entry1++)
        {
            if(m_entries[entry0] + m_entries[entry1] == 2020)
            {
                std::cout << "First product is " << m_entries[entry0] * m_entries[entry1] << '\n';
                cont = false;
                break;
            }
        }

    cont = true;
    for(size_t entry0(0); entry0 != (m_entries.size() + 1) && cont; entry0++)
        for(size_t entry1(entry0 + 1); entry1 != m_entries.size() && cont; entry1++)
            for(size_t entry2(entry1 + 1); entry2 != m_entries.size() && cont; entry2++)
            {
                if(m_entries[entry0] + m_entries[entry1] + m_entries[entry2] == 2020)
                {
                    std::cout << "Second product is " << m_entries[entry0] * m_entries[entry1] * m_entries[entry2] << '\n';
                    cont = false;
                    break;
                }
            }

}

Day1::~Day1()
{
    //dtor
}
