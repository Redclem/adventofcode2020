#include "Day21.h"

Day21::Day21(const char * data)
{
    while(*data)
    {
        m_ingredient_lists.emplace_back();
        IngredientList & cur_list = m_ingredient_lists.back();
        while(*data != '(' && *data != '\n')
        {
            const char * end = strchr(data, ' ');

            std::string ingredient(data, end);
            data = end;

            auto iter = m_ingredient_map.insert({ingredient, m_ingredient_map.size()});

            cur_list.ingredients.insert(iter.first->second);

            if(*data == ' ') data++;
        }

        if(*data == '(')
        {
            if(strncmp("(contains ", data, 10))
            {
                std::cout << "Expected \"(contains\"\n";
                return;
            }
            data += 10;

            while(*data != ')')
            {
                const char * end(data);

                while(isalpha(*end)) end++;

                std::string allergen(data, end);
                data = end;

                auto iter = m_allergen_map.insert({allergen, m_allergen_map.size()});

                cur_list.allergens.insert(iter.first->second);

                if(*data == ',') data++;
                if(*data == ' ') data++;
            }

            data++;

        }


        if(*data == '\n')
            data++;
    }
    //ctor
}

Day21::~Day21()
{
    //dtor
}

void Day21::run()
{
    if(m_allergen_map.size() > 64)
    {
        std::cout << "Too many allergens!\n";
        return;
    }

    uint64_t full_mask((uint64_t(1) << m_allergen_map.size()) - 1);

    std::vector<uint64_t> allergen_mask(m_ingredient_map.size(), full_mask);

    for(IngredientList & list : m_ingredient_lists)
    {
        uint64_t ext_mask(full_mask);
        for(uint64_t allergen : list.allergens)
        {
            ext_mask &= (full_mask ^ (uint64_t(1) << allergen));
        }


        for(auto & ingredient : m_ingredient_map)
        {
            if(list.ingredients.count(ingredient.second))
            {
                continue;
            }

            allergen_mask[ingredient.second] &= ext_mask;
        }
    }

    size_t non_allergenic_appear(0);

    for(IngredientList & recipe : m_ingredient_lists)
    {
        for(uint64_t ingredient : recipe.ingredients)
        {
            if(allergen_mask[ingredient] == 0)
                non_allergenic_appear++;
        }
    }

    std::cout << "Non allergenic ingredients appear " << non_allergenic_appear << " times\n";

    std::map<std::string, uint64_t> allergen_map;

    std::map<uint64_t, std::string> reverse_allergens;

    for(auto & allerg : m_allergen_map)
        reverse_allergens.insert({allerg.second, allerg.first});

    for(auto & ingr : m_ingredient_map)
    {
        if(allergen_mask[ingr.second])
        {
            allergen_map[ingr.first] = allergen_mask[ingr.second];
        }
    }

    std::map<std::string, std::string> ingredient_allergen_map;

    bool run(true);
    while(run)
    {
        run = false;
        for(auto & allerg : allergen_map)
        {
            uint64_t power;
            if(is_power_of_2(allerg.second, &power))
            {

                ingredient_allergen_map[reverse_allergens.at(power)] = allerg.first;

                uint64_t compl_mask = full_mask ^ allerg.second;
                for(auto & other : allergen_map)
                {
                    if(other != allerg)
                        other.second &= compl_mask;
                }
            }
            else
            {
                run = true;
            }
        }
    }

    std::cout << "Canonical dangerous list : \"";

    bool isfirst(true);

    for(auto & allerg : ingredient_allergen_map)
    {
        if(!isfirst)
        {
            std::cout << ',';
        }
        else
            isfirst = false;

        std::cout << allerg.second;
    }
    std::cout << "\"\n";
}

bool Day21::is_power_of_2(uint64_t number, uint64_t * power)
{
    if(power)
    {
        *power = 0;
        while((number & 1) == 0)
        {
            (*power)++;
            number >>= 1;
        }
    }
    else
    {
        while((number & 1) == 0)
        {
            number >>= 1;
        }
    }

    return number == 1;
}

