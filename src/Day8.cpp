#include "Day8.h"

Day8::Day8(const char * data)
{
    m_console.load_instructions(data);
}

Day8::~Day8()
{
    //dtor
}

void Day8::run()
{
    std::vector<bool> exec_instructions = m_console.run_no_rep();
    std::cout << "Accumulator before second execution of an instruction : " << m_console.get_acc() << '\n';

    for(size_t instr_index(0); instr_index != exec_instructions.size(); instr_index++)
    {
        if(!exec_instructions[instr_index]) continue;

        HandheldConsole::Instruction & instr = m_console[instr_index];

        if(instr.op == HandheldConsole::Operation::acc)
            continue;
        else if(instr.op == HandheldConsole::Operation::nop)
            instr.op = HandheldConsole::Operation::jmp;
        else if(instr.op == HandheldConsole::Operation::jmp)
            instr.op = HandheldConsole::Operation::nop;

        m_console.reset();
        if(m_console.check_exec_first_nonex())
        {
            std::cout << "Accumulator at program termination : " << m_console.get_acc() << '\n';
            return;
        }


        if(instr.op == HandheldConsole::Operation::nop)
            instr.op = HandheldConsole::Operation::jmp;
        else if(instr.op == HandheldConsole::Operation::jmp)
            instr.op = HandheldConsole::Operation::nop;
    }
}
