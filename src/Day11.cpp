#include "Day11.h"

Day11::Day11(const char * data) : m_seats(data)
{
}

Day11::~Day11()
{
    //dtor
}

void Day11::run()
{
    Map2d cpy(m_seats);
    Map2d tmp(simulate(cpy));

    while(tmp != cpy)
    {
        cpy = std::move(tmp);
        tmp = simulate(cpy);
    }

    std::cout << "Occupied seats : " << std::count(cpy.get_data().begin(), cpy.get_data().end(), '#') << '\n';

    cpy = m_seats;
    tmp = simulate(cpy);

    while(tmp != cpy)
    {
        cpy = std::move(tmp);
        tmp = simulate_non_adj(cpy);
    }

    std::cout << "Occupied seats : " << std::count(cpy.get_data().begin(), cpy.get_data().end(), '#') << '\n';
}

Map2d Day11::simulate(const Map2d & input_map)
{
    Map2d future_map(input_map.get_row_len(), input_map.get_col_len(), '.');

    for(size_t col(0);col != input_map.get_row_len(); col++)
        for(size_t row(0); row != input_map.get_col_len();row++)
            if(input_map[row][col] != '.')
                future_map[row][col] = simulate_seat(input_map, col, row);

    return future_map;
}

Map2d Day11::simulate_non_adj(const Map2d& input_map)
{
    Map2d future_map(input_map.get_row_len(), input_map.get_col_len(), '.');

    for(size_t col(0);col != input_map.get_row_len(); col++)
        for(size_t row(0); row != input_map.get_col_len();row++)
            if(input_map[row][col] != '.')
                future_map[row][col] = simulate_seat_non_adj(input_map, col, row);

    return future_map;
}


uint8_t Day11::simulate_seat(const Map2d & input_map, size_t col, size_t row)
{
    bool occ(input_map[row][col] == '#');
    uint8_t adj_occ(0);

    for(size_t i_col(col == 0 ? col : col - 1); i_col != (col == input_map.get_row_len() - 1 ? col + 1: col + 2); i_col++)
        for(size_t i_row(row == 0 ? row : row - 1); i_row != (row == input_map.get_col_len() - 1 ? row + 1: row + 2); i_row++)
        {
            if(col == i_col && row == i_row)
                continue;
            if(input_map[i_row][i_col] == '#')
                adj_occ++;
        }

    if(adj_occ == 0 && !occ) return '#';

    if(adj_occ >= 4 && occ) return 'L';

    return input_map[row][col];
}

uint8_t Day11::simulate_seat_non_adj(const Map2d & input_map, size_t col, size_t row)
{
    bool occ(input_map[row][col] == '#');
    uint8_t adj_occ(0);

    constexpr int8_t dirs[8][2] =
    {
        {1, 1},
        {0, 1},
        {-1, 1},
        {1, 0},
        {-1, 0},
        {1, -1},
        {0, -1},
        {-1, -1}
    };

    for(uint8_t dir(0); dir != 8;dir++)
    {
        size_t i_row(row), i_col(col);
        do
        {

            i_row += static_cast<size_t>(dirs[dir][0]);
            i_col += static_cast<size_t>(dirs[dir][1]);
        }
        while(i_col < input_map.get_row_len() &&
              i_row < input_map.get_col_len() &&
              input_map[i_row][i_col] == '.'
              );

        if(i_col >= input_map.get_row_len() || i_row >= input_map.get_col_len()) continue;

        if(input_map[i_row][i_col] == '#') adj_occ++;
    }
    if(adj_occ == 0 && !occ) return '#';

    if(adj_occ >= 5 && occ) return 'L';

    return input_map[row][col];

}
