#include "Day10.h"

Day10::Day10(const char * data)
{
    while(*data != 0)
    {
        char * next;
        m_adapters.push_back(strtoul(data, &next, 10));
        data = next;
        if(*data =='\n') data++;
    }
    //ctor
    std::sort(m_adapters.begin(), m_adapters.end());
}

Day10::~Day10()
{
    //dtor
}

void Day10::run()
{
    size_t jump1(0), jump3(0);

    if(m_adapters[0] == 1)
        jump1++;
    else if (m_adapters[0] == 3)
        jump3++;

    for(size_t adap(1); adap != m_adapters.size();adap++)
    {
        if(m_adapters[adap] - m_adapters[adap - 1] == 1)
            jump1++;
        else if(m_adapters[adap] - m_adapters[adap - 1] == 3)
            jump3++;
    }

    jump3++;

    number_t end_power(m_adapters.back() + 3);

    std::vector<size_t> arrang(m_adapters.size(), 0);

    std::cout << "Product of 3 and 1 difference count : " << jump1 * jump3 << '\n';

    {
        size_t adap(m_adapters.size());
        do
        {
            adap--;
            if(end_power - m_adapters[adap] <= 3)
            {
                arrang[adap]++;
                assert(arrang[adap]);
            }

            for(size_t dst_adap(adap + 1);dst_adap != m_adapters.size() && dst_adap <= adap + 3;dst_adap++)
            {
                if(m_adapters[dst_adap] - m_adapters[adap] <= 3)
                {
                    size_t before(arrang[adap]);
                    arrang[adap] += arrang[dst_adap];

                    assert(before <= arrang[adap]);
                }
            }
        }
        while(adap != 0);
    }

    size_t beg_arrang(0);

    for(size_t adap(0); adap != 3;adap++)
        if(m_adapters[adap] <= 3)
        {
            size_t before(beg_arrang);
            beg_arrang += arrang[adap];
            assert(before <= beg_arrang);
        }

    std::cout << "Beg arrang : " << beg_arrang << '\n';
}
