#include "Day17.h"

Day17::Day17(const char * data)
{
    int64_t x(0), y(0);
    while(*data)
    {
        if(*(data++) == '#')
        {
            m_3d_active_cubes.insert({x,y,0});
            m_4d_active_cubes.insert({x,y,0,0});
        }
        x++;
        if(*data == '\n')
        {
            x = 0;
            y++;
            data++;
        }
    }

    //ctor
}

Day17::~Day17()
{
    //dtor
}

void Day17::run()
{
    for(size_t cycle_num(0); cycle_num != 6; cycle_num++)
    {
        cycle();
        cycle4d();
    }

    size_t cube_count(0);

    for(auto cube(m_3d_active_cubes.begin()); cube != m_3d_active_cubes.end(); cube++)
    {
        cube_count++;
    }

    std::cout << "Cube count after 6 cycles (3d) : " << cube_count << '\n';

    cube_count = 0;

    for(auto cube(m_4d_active_cubes.begin()); cube != m_4d_active_cubes.end(); cube++)
    {
        cube_count++;
    }

    std::cout << "Cube count after 6 cycles (4d) : " << cube_count << '\n';
}

void Day17::cycle()
{
    std::map<std::array<int64_t, 3>, size_t> active_neighbor_count;

    for(const std::array<int64_t, 3> & cube : m_3d_active_cubes)
    {
        for(int8_t x_offset(-1); x_offset <= 1; x_offset++)
            for(int8_t y_offset(-1); y_offset <= 1; y_offset++)
                for(int8_t z_offset(-1); z_offset <= 1; z_offset++)
                {
                    if(!x_offset && !y_offset && !z_offset)
                    {
                        continue;
                    }
                    active_neighbor_count[ {cube[0] + x_offset, cube[1] + y_offset, cube[2] + z_offset}]++;
                }
    }

    for(auto & elem : active_neighbor_count)
    {
        if(elem.second == 3)
            m_3d_active_cubes.insert(elem.first);
    }

    for(auto iter(m_3d_active_cubes.begin()); iter != m_3d_active_cubes.end(); iter++)
    {
        size_t neighbors = active_neighbor_count[*iter];

        while(neighbors != 2 && neighbors != 3)
        {
            iter = m_3d_active_cubes.erase(iter);
            neighbors = active_neighbor_count[*iter];
        }
    }
}

void Day17::cycle4d()
{
    std::map<std::array<int64_t, 4>, size_t> active_neighbor_count;

    for(const std::array<int64_t, 4> & cube : m_4d_active_cubes)
    {
        for(int8_t x_offset(-1); x_offset <= 1; x_offset++)
            for(int8_t y_offset(-1); y_offset <= 1; y_offset++)
                for(int8_t z_offset(-1); z_offset <= 1; z_offset++)
                    for(int8_t w_offset(-1); w_offset <= 1; w_offset++)
                    {
                        if(!x_offset && !y_offset && !z_offset && !w_offset)
                        {
                            continue;
                        }
                        active_neighbor_count[ {cube[0] + x_offset, cube[1] + y_offset, cube[2] + z_offset, cube[3] + w_offset}]++;
                    }
    }

    for(auto & elem : active_neighbor_count)
    {
        if(elem.second == 3)
            m_4d_active_cubes.insert(elem.first);
    }

    for(auto iter(m_4d_active_cubes.begin()); iter != m_4d_active_cubes.end(); iter++)
    {
        size_t neighbors = active_neighbor_count[*iter];

        while(neighbors != 2 && neighbors != 3)
        {
            iter = m_4d_active_cubes.erase(iter);
            neighbors = active_neighbor_count[*iter];
        }
    }
}

