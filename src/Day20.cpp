#include "Day20.h"

Day20::Day20(const char * data)
{
    while(*data)
    {
        if(strncmp(data, "Tile ", 5) != 0)
        {
            std::cout << "Expected tile\n";
            return;
        }

        data += 5;

        char * next;
        uint64_t tile_index(strtoull(data, &next, 10));
        data = next;

        if(*data == ':') data++;
        if(*data == '\n') data++;

        Tile & newtile = m_tiles[tile_index];
        data = newtile.load(data);


        if(*data == '\n')
            data++;
    }

    double side = sqrt(double(m_tiles.size()));
    double intpart;
    if(modf(side, &intpart) != 0.0)
    {
        std::cout << "Expected integer root\n";
        return;
    }

    m_image_side_length = size_t(side);
}

Day20::~Day20()
{
    //dtor
}

void Day20::run()
{
    bool run = true;
    auto beg_tile = m_tiles.begin();
    std::vector<Tile> image_tiles;
    while(run && beg_tile != m_tiles.end())
    {
        for(uint8_t orien(0); orien != 16; orien++)
        {
            uint64_t res = try_tile(image_tiles, beg_tile->first, orien);
            if(res)
            {
                std::cout << "Id product : " << res << '\n';
                run = false;
                break;
            }
        }
        beg_tile++;
    }

    Image img;
    img.load_tiles(image_tiles, m_image_side_length);

    std::cout << "Roughness : " << img.roughness() << "\n\n";
}

uint64_t Day20::try_tile(std::vector<Tile> & tiles, uint64_t first_tile, uint8_t orient)
{
    uint64_t prod(first_tile);

    std::set<uint64_t> used_tiles({first_tile});

    std::vector<Tile> & last_tiles = tiles;
    last_tiles.clear();
    last_tiles.push_back(m_tiles[first_tile]);
    last_tiles.back().orient(static_cast<int8_t>(orient));


    while(used_tiles.size() != m_tiles.size())
    {
        bool success(false);
        for(const auto & tile : m_tiles)
        {
            if(used_tiles.count(tile.first))
                continue;

            Tile cpy(tile.second);

            if(used_tiles.size() % m_image_side_length)
            {
                do
                {
                    if(last_tiles.back().match_right(cpy))
                    {
                        if(last_tiles.size() >= m_image_side_length)
                        {
                            if(last_tiles[last_tiles.size() - m_image_side_length].match_bottom(cpy))
                            {
                                success = true;
                                break;
                            }
                            else
                            {
                                cpy.disp();
                            }
                        }
                        else
                        {
                            success = true;
                            break;
                        }
                    }
                    cpy.orient(1);
                }
                while(cpy.get_orientation() != 0);
            }
            else
            {
                do
                {
                    if(last_tiles[last_tiles.size() - m_image_side_length].match_bottom(cpy))
                    {
                        success = true;
                        break;
                    }
                    cpy.orient(1);
                }
                while(cpy.get_orientation() != 0);
            }

            if(success)
            {
                if(used_tiles.size() == m_image_side_length - 1 ||
                        used_tiles.size() == (m_image_side_length - 1) * m_image_side_length ||
                        used_tiles.size() == (m_image_side_length - 1) * (m_image_side_length + 1))
                {
                    prod *= tile.first;
                }

                last_tiles.push_back(cpy);

                used_tiles.insert(tile.first);
                break;
            }
        }
        if(!success)
            return 0;
    }
    return prod;
}
