#include "Day6.h"

Day6::Day6(const char* data)
{
    Form current_form;
    while(*data != 0)
    {
        bool answers[26] = {};
        while(isalpha(*data))
        {
            answers[*data - 'a'] = true;
            data++;
        }

        for(uint8_t ans(0); ans != 26; ans++)
        {
            if(answers[ans])
                current_form[ans] |= Form::Answer::ANSWER_SOMEONE;
            else
                current_form[ans] &= Form::Answer::ANSWER_SOMEONE;
        }

        if(*data == '\n')
            if(*(++data) == '\n')
            {
                m_forms.push_back(current_form);
                current_form = Form();
                data++;
            }
    }
    m_forms.push_back(current_form);
}

Day6::~Day6()
{
    //dtor
}

void Day6::run()
{
    uint32_t answer_count(0);
    uint32_t everyone_answer_count(0);
    for(const Form & form : m_forms)
    {
        for(uint8_t answer(0); answer != 26; answer++)
        {
            if(form[answer] != Form::Answer::ANSWER_NONE)
            {
                answer_count++;
                if((form[answer] & Form::Answer::ANSWER_EVERYONE) != Form::Answer::ANSWER_NONE)
                    everyone_answer_count++;
            }
        }
    }
    std::cout << "Answer count sum : " << answer_count << '\n';
    std::cout << "Everyone answered count sum : " << everyone_answer_count << '\n';
}
