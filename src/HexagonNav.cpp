#include "HexagonNav.h"

HexagonNav::HexagonNav() : m_x(0), m_y(0)
{
    //ctor
}

void HexagonNav::move_dir(Direction dir)
{
    switch(dir)
    {
    case Direction::e:
        m_x+=2;
        break;
    case Direction::w:
        m_x-=2;
        break;
    case Direction::ne:
        m_y++;
        m_x++;
        break;
    case Direction::nw:
        m_y++;
        m_x--;
        break;
    case Direction::se:
        m_y--;
        m_x++;
        break;
    case Direction::sw:
        m_y--;
        m_x--;
        break;
    }
}
