#include "Day14.h"

Day14::Day14(const char * data)
{
    uint64_t andmask(0);
    uint64_t ormask(0);

    while(*data)
    {
        char buffer[4];
        memcpy(buffer, data, 3);
        buffer[3] = 0;
        data += 3;
        if(strcmp(buffer, "mas") == 0)
        {
            andmask = 0;
            ormask = 0;

            data += 4;

            for(size_t iter(0);iter != 36;iter++)
            {
                andmask <<= 1;
                ormask <<= 1;
                switch(*data)
                {
                case '0':
                    break;
                case 'X':
                    andmask |= 1;
                    break;
                case '1':
                    andmask |= 1;
                    ormask |= 1;
                    break;
                default:
                    std::cout << "Unknown symbol : " << *data << '\n';
                }
                data++;

            }
        }
        else if (strcmp(buffer, "mem") == 0)
        {
            // Write version 1
            if(*data != '[')
            {
                std::cout << "Error in input\n";
                return;
            }

            data++;

            char * next;
            size_t addr = strtoull(data, &next, 10);
            data = next;

            if(*data != ']')
            {
                std::cout << "Error in input\n";
                return;
            }

            data += 4;

            uint64_t val = strtoull(data, &next, 10);

            data = next;

            access_mem(addr) = (val & andmask) | ormask;

            // Write version 2

            uint64_t flbits(andmask ^ ormask);

            write_float((addr | ormask) & (~flbits), flbits, val);

        }

        if(*data == '\n') data++;
    }
    //ctor
}

void Day14::write_float(uint64_t address, uint64_t floatbits, uint64_t val)
{
    if(floatbits == 0)
    {
        m_version2_mem[address] = val;
        return;
    }

    for(uint64_t checked_bit(uint64_t(1) << 35); checked_bit; checked_bit >>= 1)
    {
        if(checked_bit & floatbits)
        {
            write_float(address | checked_bit, floatbits ^ checked_bit, val);
            write_float(address, floatbits ^ checked_bit, val);
            return;
        }
    }

    std::cout << "Failure to write with floating bits\n";
}


Day14::~Day14()
{
    //dtor
}

void Day14::run()
{
    uint64_t sum(0);
    for(uint64_t val : m_memory)
    {
        sum += val;
    }
    std::cout << "Sum is : " << sum << '\n';

    sum = 0;

    for(auto & mem_loc : m_version2_mem)
    {
        sum += mem_loc.second;
    }

    std::cout << "Version 2 sum : " << sum << '\n';
}

uint64_t& Day14::access_mem(size_t index)
{
    if(index >= m_memory.size())
        m_memory.resize(index + 1, 0);
    return m_memory[index];
}

