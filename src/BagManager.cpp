#include "BagManager.h"

void BagManager::load_rules(const char* rules)
{
    while(*rules != 0)
    {
        const char * first_adj_end = strchr(rules, ' ');
        std::string first_adj(rules, first_adj_end);
        rules = first_adj_end + 1;
        const char * second_adj_end = strchr(rules, ' ');
        std::string second_adj(rules, second_adj_end);
        rules = second_adj_end + 1;

        BagId id = {get_first_adj_id(first_adj), get_second_adj_id(second_adj)};

        rules = strchr(strchr(rules, ' ') + 1, ' ') + 1;

        if(strncmp(rules, "no other bags", 13) == 0)
        {
            rules += 13;
        }
        else
        {
            std::vector<BagCount> & bags_inside = m_bags[id];

            while(*rules != '.')
            {
                if(*rules == ',') rules++;
                BagCount cnt;
                char * next;
                cnt.bag_count = static_cast<uint16_t>(strtoul(rules, &next, 10));
                rules = next + 1;

                const char * end_first = strchr(rules, ' ');
                std::string first(rules, end_first);
                rules = end_first + 1;

                const char * end_second = strchr(rules, ' ');
                std::string second(rules, end_second);
                rules = end_second + 1;

                cnt.bag = {get_first_adj_id(first), get_second_adj_id(second)};

                bags_inside.push_back(cnt);

                if(strncmp(rules, "bag", 3) == 0) rules += 3;
                if(*rules == 's') rules++;
            }
        }

        rules++;
        if(*rules == '\n') rules++;
    }
}

bool BagManager::contains(BagId container, BagId contained) const
{
    auto iter = m_bags.find(container);

    if(iter == m_bags.end())
        return false;

    const std::vector<BagCount> & contained_bags = iter->second;

    for(const BagCount & bags_inside : contained_bags)
    {
        if(bags_inside.bag == contained)
            return true;
        else if(contains(bags_inside.bag, contained))
            return true;
    }
    return false;
}

size_t BagManager::count_contain(BagId bag) const
{
    size_t count(0);
    for(const auto & container : m_bags)
    {
        if(contains(container.first, bag))
            count++;
    }
    return count;
}

size_t BagManager::count_inside(BagId container) const
{
    auto iter = m_bags.find(container);

    if(iter == m_bags.end())
        return 0;

    size_t count(0);
    const std::vector<BagCount> & contained_bags = iter->second;

    for(const BagCount & contained : contained_bags)
    {
        count += contained.bag_count + contained.bag_count * count_inside(contained.bag);
    }
    return count;
}

