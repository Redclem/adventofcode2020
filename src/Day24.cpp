#include "Day24.h"

Day24::Day24(const char * data)
{
    m_directions.emplace_back();
    while(*data)
    {
        switch(*data)
        {
        case 'w':
            m_directions.back().push_back(HexagonNav::Direction::w);
            break;
        case 'e':
            m_directions.back().push_back(HexagonNav::Direction::e);
            break;
        case 'n':
            data++;
            if(*data == 'e')
                m_directions.back().push_back(HexagonNav::Direction::ne);
            else if (*data == 'w')
                m_directions.back().push_back(HexagonNav::Direction::nw);
            else
            {
                std::cout << "Unknown direction\n";
            }
            break;
        case 's':
            data++;
            if(*data == 'e')
                m_directions.back().push_back(HexagonNav::Direction::se);
            else if (*data == 'w')
                m_directions.back().push_back(HexagonNav::Direction::sw);
            else
            {
                std::cout << "Unknown direction\n";
            }
            break;
        default:
            std::cout << "Unknown instruction\n";
        }

        data++;

        if(*data == '\n')
        {
            m_directions.back().shrink_to_fit();
            m_directions.emplace_back();
            data++;
        }
    }
}

Day24::~Day24()
{
    //dtor
}

void Day24::run()
{
    HexagonTiling tiling;
    for(auto & directions : m_directions)
    {
        HexagonNav cur_nav;
        for(HexagonNav::Direction dir : directions)
        {
            cur_nav.move_dir(dir);
        }

        tiling.flip(cur_nav);
    }

    std::cout << tiling.count_flipped() << " tiles are left with the black side up\n";

    for(size_t i(0); i != 100;i++)
    {
        tiling.flip_art();
    }

    std::cout << tiling.count_flipped() << " tiles are left with the black side up at day 100\n";

}
