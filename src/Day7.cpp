#include "Day7.h"

Day7::Day7(const char * data)
{
    m_bag_manager.load_rules(data);
    //ctor
}

Day7::~Day7()
{
    //dtor
}

void Day7::run()
{
    std::cout << m_bag_manager.count_contain({m_bag_manager.get_first_adj_id("shiny"), m_bag_manager.get_second_adj_id("gold")}) << " bags contain a shiny gold bag\n";
    std::cout << m_bag_manager.count_inside({m_bag_manager.get_first_adj_id("shiny"), m_bag_manager.get_second_adj_id("gold")}) << " bags are required inside a shiny gold bag\n";
}
