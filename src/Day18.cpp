#include "Day18.h"

Day18::Day18(const char * data)
{
    m_expressions.emplace_back();

    while(*data)
    {
        while(!isspace(*data))
        {
            m_expressions.back().push_back(*data);
            data++;
        }

        if(*data == '\n')
        {
            m_expressions.back().shrink_to_fit();
            m_expressions.emplace_back();
        }

        if(*data)
            data++;
    }
    //ctor
}

Day18::~Day18()
{
    //dtor
}

void Day18::run()
{
    uint64_t sum(0);
    uint64_t sum_prio(0);
    for(std::string & expr : m_expressions)
    {
        const char * value = expr.c_str();
        sum += evaluate_expression(value);
        value = expr.c_str();
        sum_prio += evaluate_expression_prio(value);
    }

    std::cout << "Sum of expressions : " << sum << '\n';

    std::cout << "Sum of expressions with new rules : " << sum_prio << '\n';
}

uint64_t Day18::evaluate_expression(const char* & txt)
{
    uint64_t val = evaluate_val(txt);
    while(*txt != ')' && *txt != '\n' && *txt)
    {
        char op = *(txt++);
        uint64_t rhs = evaluate_val(txt);
        switch(op)
        {
        case '+':
            val += rhs;
            break;
        case '*':
            val *= rhs;
            break;
        }
    }

    if(*txt == ')')
        txt++;
    return val;
}

uint64_t Day18::evaluate_val(const char*& txt)
{
    if(isdigit(*txt))
    {
        char * next;
        uint64_t val(strtoull(txt, &next, 10));
        txt = next;
        return val;
    }
    else if (*txt == '(')
    {
        txt++;
        return evaluate_expression(txt);
    }
    else
    {
        std::cout << "Invalid value\n";
        return 0;
    }
}

uint64_t Day18::evaluate_expression_prio(const char*& txt)
{
    std::vector<uint64_t> values(1, evaluate_val_prio(txt));

    while(*txt != ')' && *txt != '\n' && *txt)
    {
        char op = *(txt++);
        uint64_t rhs = evaluate_val_prio(txt);
        switch(op)
        {
        case '+':
            values.back() += rhs;
            break;
        case '*':
            values.push_back(rhs);
            break;
        }
    }

    if(*txt == ')')
        txt++;

    uint64_t res(values.front());

    for(uint64_t iter(1); iter != values.size();iter++)
        res *= values[iter];

    return res;
}

uint64_t Day18::evaluate_val_prio(const char*& txt)
{
    if(isdigit(*txt))
    {
        char * next;
        uint64_t val(strtoull(txt, &next, 10));
        txt = next;
        return val;
    }
    else if (*txt == '(')
    {
        txt++;
        return evaluate_expression_prio(txt);
    }
    else
    {
        std::cout << "Invalid value\n";
        return 0;
    }
}
