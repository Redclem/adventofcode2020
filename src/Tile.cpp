#include "Tile.h"

Tile::Tile() : m_orientation(0)
{
    //ctor
}

Tile::~Tile()
{
    //dtor
}

Tile::Tile(const char* data) : m_orientation(0)
{
    load(data);
}

const char * Tile::load(const char* data)
{
    m_orientation = 0;
    for(size_t input(0);input != 100;input++)
    {
        m_data[input] = *(data++);
        if(input % 10 == 9 && *data == '\n')
            data++;
    }
    return data;
}

Tile::ConstTileSub Tile::operator[](size_t index) const
{
    bool rev_x(m_orientation & 2);
    bool rev_y(m_orientation & 4);

    if(rev_x) index = 9 - index;

    ptrdiff_t stride = rev_y ? -1 : 1;
    size_t offset = rev_y ? 9 : 0;

    if(m_orientation % 2)
    {
        return {m_data + index + 10 * offset, 10 * stride};
    }
    else
    {
        return {m_data + 10 * index + offset, 1 * stride};
    }
}

Tile::TileSub Tile::operator[](size_t index)
{
    bool rev_x(m_orientation & 2);
    bool rev_y(m_orientation & 4);

    if(rev_x) index = 9 - index;

    ptrdiff_t stride = rev_y ? -1 : 1;
    size_t offset = rev_y ? 9 : 0;

    if(m_orientation % 2)
    {
        return {m_data + index + 10 * offset, 10 * stride};
    }
    else
    {
        return {m_data + 10 * index + offset, 1 * stride};
    }
}

void Tile::disp() const
{
    for(size_t iter(0);iter != 100;iter++)
    {
        std::cout << operator[](iter / 10)[iter % 10];
        if(iter % 10 == 9)
            std::cout << '\n';
    }
}

bool Tile::match_right(const Tile& right)
{
    for(uint8_t iter(0);iter != 10;iter++)
    {
        if(operator[](iter)[9] != right[iter][0])
            return false;
    }
    return true;
}

bool Tile::match_bottom(const Tile& bottom)
{
    ConstTileSub a(operator[](9)), b(bottom[0]);
    for(uint8_t iter(0);iter != 10;iter++)
    {
        if(a[iter] != b[iter])
            return false;
    }
    return true;
}

Tile::Tile(const Tile& from) : m_orientation(from.m_orientation)
{
    for(size_t iter(0);iter != 100;iter++)
        m_data[iter] = from.m_data[iter];
}

Tile& Tile::operator=(const Tile& rhs)
{
    m_orientation = rhs.m_orientation;
    for(size_t iter(0);iter != 100;iter++)
        m_data[iter] = rhs.m_data[iter];
    return *this;
}

