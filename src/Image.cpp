#include "Image.h"

Image::Image() : m_side_len(0)
{
    //ctor
}

Image::~Image()
{
    //dtor
}

void Image::load_tiles(std::vector<Tile>& tiles, size_t side_tiles)
{
    m_side_len = side_tiles * 8;

    m_data.resize(m_side_len * m_side_len);

    for(size_t row(0); row != m_side_len; row++)
    {
        for(size_t col(0); col != m_side_len; col++)
        {
            Tile & tile = tiles[(row / 8) * side_tiles + (col / 8)];

            m_data[row * m_side_len + col] = tile[1 + (row % 8)][1 + (col % 8)];
        }
    }
}

void Image::disp() const
{
    for(size_t iter(0); iter != m_data.size(); iter++)
    {
        std::cout << at(iter / m_side_len, iter % m_side_len);

        if((iter % m_side_len) == (m_side_len - 1))
        {
            std::cout << '\n';
        }
    }
}

bool Image::try_match(size_t row, size_t col)
{
    if(col + sea_monster_width > m_side_len)
        return false;
    if(row + sea_monster_height > m_side_len)
        return false;

    for(size_t x(0); x != sea_monster_width; x++)
        for(size_t y(0); y != sea_monster_height; y++)
            if(sea_monster[y][x] == '#')
                if(at(row + y, col + x) == '.')
                    return false;

    for(size_t x(0); x != sea_monster_width; x++)
        for(size_t y(0); y != sea_monster_height; y++)
            if(sea_monster[y][x] == '#')
                at(row + y, col + x) = 'O';

    return true;
}

uint64_t Image::roughness()
{
    size_t match_count(0);

    do
    {
        m_orient = (m_orient + 1) % 8;

        for(size_t iter(0); iter != m_data.size(); iter++)
            if(try_match(iter / m_side_len, iter % m_side_len))
                match_count++;
    }
    while(match_count == 0 && m_orient);

    if(m_orient == 0 && match_count == 0)
        std::cout << "Warning! No monster detected in all orientations!\n";

    uint64_t roughness = uint64_t(std::count(m_data.begin(), m_data.end(), '#'));

    return roughness;
}

const char& Image::at(size_t row, size_t col) const
{
    if(m_orient & 1)
        std::swap(row, col);

    if(m_orient & 2)
        row = m_side_len - 1 - row;

    if(m_orient & 4)
        col = m_side_len - 1 - col;

    return m_data[row * m_side_len + col];
}

char& Image::at(size_t row, size_t col)
{
    if(m_orient & 1)
        std::swap(row, col);

    if(m_orient & 2)
        row = m_side_len - 1 - row;

    if(m_orient & 4)
        col = m_side_len - 1 - col;

    return m_data[row * m_side_len + col];
}
