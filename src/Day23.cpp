#include "Day23.h"

Day23::Day23(const char * data)
{
    m_cups.load(data);
}

Day23::~Day23()
{
    //dtor
}

void Day23::run()
{
    Cups cpy = m_cups;

    for(size_t i(0);i != 100;i++)
        m_cups.round();

    std::cout << "Labels after cup 1 : ";

    m_cups.print_after_1();

    m_cups = cpy;

    m_cups.gen_cups(1000000);

    for(size_t i(0);i != 10000000;i++)
        m_cups.round();

    std::cout << "Product of 2 after cup 1 : " << m_cups.prod_2() << '\n';
}


