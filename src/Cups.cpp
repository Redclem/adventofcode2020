#include "Cups.h"

Cups::Cups()
{
    //ctor
}

void Cups::load(const char* data)
{
    m_cups.resize(strlen(data) + 1);

    cup_t prev_cup = cup_t(*(data++) - '0');
    cup_t first_cup = prev_cup;
    m_current_cup = first_cup;

    while(*data)
    {
        m_cups[prev_cup] = cup_t(*(data++) - '0');
        prev_cup = m_cups[prev_cup];
    }

    m_cups[prev_cup] = first_cup;

    m_last_cup = prev_cup;
}

void Cups::gen_cups(size_t cup_count)
{
    m_cups.reserve(cup_count + 1);
    while(cup_count >= m_cups.size())
    {
        cup_t temp = m_cups[m_last_cup];
        m_cups[m_last_cup] = m_cups.size();
        m_last_cup = m_cups.size();
        m_cups.push_back(temp);
    }
}

void Cups::round()
{
    cup_t removed[3];
    cup_t current = m_current_cup;
    for(size_t i(0);i != 3;i++)
    {
        removed[i] = m_cups[current];
        current = removed[i];
    }

    cup_t dest(m_current_cup == 1 ? m_cups.size() - 1 : m_current_cup - 1);

    while(removed[0] == dest ||
          removed[1] == dest ||
          removed[2] == dest)
    {
        dest = (dest == 1 ? m_cups.size() - 1 : dest - 1);
    }

    cup_t tmp = m_cups[removed[2]];

    m_cups[removed[2]] = m_cups[dest];
    m_cups[dest] = removed[0];

    m_cups[m_current_cup] = tmp;

    m_current_cup = tmp;

}

void Cups::print_cups()
{
    size_t iter(1);
    do
    {
        std::cout << m_cups[iter] << ' ';
        iter = m_cups[iter];
    }
    while(iter != 1);

    std::cout << '\n';
}

void Cups::print_after_1()
{
    size_t iter(1);
    do
    {
        if(m_cups[iter] == 1)
            break;
        std::cout << m_cups[iter];
        iter = m_cups[iter];
    }
    while(iter != 1);

    std::cout << '\n';
}

Cups::cup_t Cups::prod_2()
{
    cup_t prod(m_cups[1]);

    prod *= m_cups[m_cups[1]];

    return prod;
}



