#include "Day3.h"

Day3::Day3(const char * data) : m_map(data)
{
}

Day3::~Day3()
{
    //dtor
}

void Day3::run()
{
    {
        size_t x(0), y(0);
        size_t trees(0);
        do
        {
            x+=3;
            y++;
            if(m_map[y][x] == '#')
                trees++;
        }
        while(y <= m_map.get_col_len());

        std::cout << trees << " trees encountered\n";
    }

    constexpr uint32_t dirs[5][2] = {{1, 1}, {3, 1}, {5, 1}, {7, 1}, {1, 2}};

    size_t prod(1);

    for(uint32_t dir(0); dir != 5; dir++)
    {
        size_t dir_tree_count(0);
        size_t x(0), y(0);
        do
        {
            x += dirs[dir][0];
            y += dirs[dir][1];

            if(m_map[y][x] == '#')
                dir_tree_count++;
        }
        while(y <= m_map.get_col_len());

        prod *= dir_tree_count;
    }

    std::cout << "Product of encountered trees : " << prod << '\n';
}

