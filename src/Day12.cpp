#include "Day12.h"

Day12::Day12(const char * data)
{
    Instruction instr;
    while(*data)
    {
        instr.chr = *(data++);

        char * next;

        instr.value = strtoul(data, &next, 10);

        data = next;

        if(*data == '\n')
            data++;

        m_instructions.push_back(instr);
    }
    //ctor
}

Day12::~Day12()
{
    //dtor
}

void Day12::run()
{
    for(Instruction & instr : m_instructions)
        m_ferry.exec_instr(instr.chr, instr.value);

    std::cout << "Manhattan after running instructions : " << m_ferry.manhattan() << '\n';

    m_ferry.reset();

    for(Instruction & instr : m_instructions)
        m_ferry.exec_instr_way(instr.chr, instr.value);

    std::cout << "Manhattan after running instructions in waypoint mode : " << m_ferry.manhattan() << '\n';
}
