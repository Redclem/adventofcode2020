#include "Map2d.h"

Map2d::Map2d() : m_row_len(0)
{
    //ctor
}

Map2d::Map2d(const char* data) : m_row_len(0)
{
    read(data);
}

Map2d::Map2d(size_t cols, size_t rows, uint8_t init_state) : m_data(rows * cols, init_state), m_row_len(cols)
{
}

void Map2d::read(const char* data)
{
    while(*data != 0)
    {
        m_data.push_back(static_cast<uint8_t>(*data));
        data++;
        if(*data == '\n')
        {
            data++;
            if(m_row_len == 0)
                m_row_len = m_data.size();
        }
    }
}

Map2d::Map2d(Map2d&& from) : m_data(std::move(from.m_data)), m_row_len(from.m_row_len)
{

}

Map2d& Map2d::operator=(Map2d&& rhs)
{
    m_row_len = rhs.m_row_len;
    m_data = std::move(rhs.m_data);
    return *this;
}

void Map2d::disp()
{
    for(size_t i(0);i != m_data.size();i++)
    {
        std::cout << static_cast<char>(m_data[i]);
        if(i % m_row_len == m_row_len - 1)
            std::cout << '\n';
    }
}

Map2d::Map2d(const Map2d& from) : m_data(from.m_data), m_row_len(from.m_row_len)
{

}

Map2d& Map2d::operator=(const Map2d& rhs)
{
    m_data = rhs.m_data;
    m_row_len = rhs.m_row_len;
    return *this;
}


Map2d::~Map2d()
{
    //dtor
}

