#include "Day4.h"

Day4::Day4(const char * data)
{
    Passport cur_passport = {};
    while(*data != 0)
    {
        char field_type[3] = {*(data++), *(data++), *(data++)};

        if(*data == ':')
            data++;

        IF_MATCH(byr) /** EYR */
        {
            REGISTER_PRESENT(byr);

            uint16_t year(0);
            for(uint8_t digit(0); digit != 4; digit++)
                year = static_cast<uint16_t>(year * 10 + (*(data++) - '0'));

            if(year >= 1920 && year <= 2002)
                REGISTER_VALID(byr);
        }
        ELSE_IF_MATCH(iyr) /** IYR */
        {
            REGISTER_PRESENT(iyr);

            uint16_t year(0);
            for(uint8_t digit(0); digit != 4; digit++)
                year = static_cast<uint16_t>(year * 10 + (*(data++) - '0'));

            if(year >= 2010 && year <= 2020)
                REGISTER_VALID(iyr);
        }
        ELSE_IF_MATCH(eyr) /** EYR */
        {
            REGISTER_PRESENT(eyr);

            uint16_t year(0);
            for(uint8_t digit(0); digit != 4; digit++)
                year = static_cast<uint16_t>(year * 10 + (*(data++) - '0'));

            if(year >= 2020 && year <= 2030)
                REGISTER_VALID(eyr);
        }
        ELSE_IF_MATCH(hgt) /** HGT */
        {
            REGISTER_PRESENT(hgt);

            uint32_t height(0);
            while(isdigit(*data))
            {
                height = height * 10 + static_cast<uint32_t>(*(data++) - '0');
            }

            if(!isspace(*data))
            {

                char unit[2] = {*(data++), *(data++)};

                if(memcmp(unit, "cm", 2) == 0)
                {
                    if(height >= 150 && height <= 193)
                        REGISTER_VALID(hgt);
                }
                else if(memcmp(unit, "in", 2) == 0)
                {
                    if(height >= 59 && height <= 76)
                        REGISTER_VALID(hgt);
                }
            }
        }
        ELSE_IF_MATCH(hcl) /** HCL */
        {
            REGISTER_PRESENT(hcl);
            if(*data == '#')
            {
                data++;
                bool valid(true);
                for(uint8_t dgt(0); dgt != 6; dgt++)
                {
                    if(!isxdigit(*data))
                    {
                        valid = false;
                        break;
                    }
                    data++;
                }
                if(valid)
                    REGISTER_VALID(hcl);
            }
        }
        ELSE_IF_MATCH(ecl)
        {
            REGISTER_PRESENT(ecl);

            const char * clrs = "ambblubrngrygrnhzloth";

            bool valid(false);

            while(*clrs)
            {
                if(memcmp(data, clrs, 3) == 0)
                {
                    valid = true;
                    break;
                }
                clrs += 3;
            }

            if(valid) REGISTER_VALID(ecl);
        }
        ELSE_IF_MATCH(pid)
        {
            REGISTER_PRESENT(pid);


            uint32_t digit_count(0);

            while(isdigit(*data))
            {
                digit_count++;
                data++;
            }

            if(digit_count == 9)
                REGISTER_VALID(pid);
        }

        while(!isspace(*data) && *data) data++;

        if(*data == '\n')
        {
            data++;
            if(*data == '\n')
            {
                m_passports.push_back(cur_passport);
                cur_passport = {};
                data++;
            }
        }
        else if (*data == ' ')
        {
            data++;
        }
    }
    m_passports.push_back(cur_passport);
    //ctor
}

Day4::~Day4()
{
    //dtor
}

void Day4::run()
{
    size_t all_present_count(0);
    size_t valid_count(0);
    for(size_t pass(0); pass != m_passports.size(); pass++)
    {
        if(m_passports[pass].present_fields == Passport::Fields(255) || m_passports[pass].present_fields == Passport::Fields(127))
        {
            all_present_count++;
            if(m_passports[pass].valid_fields == Passport::Fields(255) || m_passports[pass].valid_fields == Passport::Fields(127))
                valid_count++;
        }
    }
    std::cout << all_present_count << " valid passports over " << m_passports.size() << " passports total for first part\n";
    std::cout << valid_count << " passports valid for second part\n";
}
