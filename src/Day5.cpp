#include "Day5.h"

Day5::Day5(const char * data)
{
    while(*data != 0)
    {
        Seat current_seat;
        for(uint8_t chr(0); chr != 7;chr++)
        {
            current_seat.row = static_cast<uint8_t>((current_seat.row << 1) + (*(data++) == 'B' ? 1 : 0));
        }

        for(uint8_t chr(0); chr != 3;chr++)
        {
            current_seat.col = static_cast<uint8_t>((current_seat.col << 1) + (*(data++) == 'R' ? 1 : 0));
        }
        m_seats.emplace(current_seat.row * 8 + current_seat.col, current_seat);
        if(*data == '\n') data++;
    }
    //ctor
}

Day5::~Day5()
{
    //dtor
}

void Day5::run()
{
    std::cout << "Max seat id is " << (--m_seats.end())->first << '\n';

    uint16_t cur_id = 7;
    auto cur_seat = m_seats.find(cur_id);

    while(cur_seat == m_seats.end()) /** Ignore first seats who are not found */
    {
        cur_id++;
        cur_seat = m_seats.find(cur_id);
    }

    while(cur_seat->first < 1016 && cur_seat != m_seats.end())
    {
        auto prev_seat = cur_seat++;
        cur_id++;
        if(cur_seat->first != cur_id)
        {
            if(prev_seat->first == cur_id - 1 && cur_seat->first == cur_id + 1)
            {
                std::cout << "Your set id : " << cur_id;
            }
            cur_id = cur_seat->first;
        }
    }
}

