#include "Day2.h"

Day2::Day2(const char * data)
{
    char * next;
    while(data != nullptr)
    {
        PasswordAndPolicy next_pass;
        next_pass.first_rule_parameter = strtoul(data, &next, 10);
        next_pass.second_rule_parameter = strtoul(next + 1, &next, 10);
        next_pass.criterium_char = *(next + 1);
        data = (next + 4);
        const char * end = strchr(data, '\n');
        if(end)
        {
            next_pass.password = std::string(data,end);
        }
        else
        {
            next_pass.password = std::string(data);
        }
        data = end;
        //std::cout << next_pass.min_appear << '-' << next_pass.max_appear << ' ' << next_pass.criterium_char << ": " << next_pass.password << '\n';
        m_passwords.push_back(next_pass);
    }
    //ctor
}

Day2::~Day2()
{
    //dtor
}

void Day2::run()
{
    uint32_t valid_count_first(0), valid_count_second(0);
    for(PasswordAndPolicy & cur_pas : m_passwords)
    {
        auto count = std::count(cur_pas.password.begin(), cur_pas.password.end(), cur_pas.criterium_char);
        if(count <= cur_pas.second_rule_parameter && count >= cur_pas.first_rule_parameter)
            valid_count_first++;
        if((cur_pas.password[cur_pas.first_rule_parameter - 1] == cur_pas.criterium_char) != (cur_pas.password[cur_pas.second_rule_parameter - 1] == cur_pas.criterium_char))
            valid_count_second++;
    }
    std::cout << valid_count_first << " valid passwords to first part\n";
    std::cout << valid_count_second << " valid passwords to second part\n";
}
