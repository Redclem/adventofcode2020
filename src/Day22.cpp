#include "Day22.h"

Day22::Day22(const char * data)
{
    if(strncmp(data, "Player 1:", 9))
    {
        std::cout << "Expected\"Player 1:\"\n";
        return;
    }

    data += 9;

    if(*data == '\n') data++;

    while(*data != '\n')
    {
        char * next;
        m_player1_cards.push_back(uint16_t(strtoull(data, &next, 10)));
        data = next;
        if(*data == '\n')
            data++;
    }

    data++;

    if(strncmp(data, "Player 2:", 9))
    {
        std::cout << "Expected\"Player 2:\"\n";
        return;
    }

    data += 9;

    while(*data)
    {
        char * next;
        m_player2_cards.push_back(uint16_t(strtoul(data, &next, 10)));
        data = next;
        if(*data == '\n')
            data++;
    }
    //ctor
}

Day22::~Day22()
{
    //dtor
}

void Day22::run()
{
    uint32_t winner(0);
    uint64_t winner_score(0);

    std::deque<uint16_t> cpy1(m_player1_cards);
    std::deque<uint16_t> cpy2(m_player2_cards);

    size_t round(0);

    while(!winner)
    {
        round++;

        winner = play_round();
    }

    std::cout << std::dec;

    std::deque<uint16_t> * winner_stack(nullptr);

    if(winner == 1)
        winner_stack = &m_player1_cards;
    else
        winner_stack = &m_player2_cards;

    size_t mul(1);

    while(!winner_stack->empty())
    {
        winner_score += mul * winner_stack->back();
        winner_stack->pop_back();
        mul++;
    }

    std::cout << "Winner's score at Combat : " << winner_score << '\n';

    winner = play_recursive_combat(cpy1, cpy2, &winner_score);

    std::cout << "Winner's score at Recursive Combat : " << winner_score << '\n';
}

uint32_t Day22::play_round()
{
    uint16_t card_pl1(m_player1_cards.front()), card_pl2(m_player2_cards.front());
    m_player1_cards.pop_front();
    m_player2_cards.pop_front();

    if(card_pl1 > card_pl2)
    {
        m_player1_cards.push_back(card_pl1);
        m_player1_cards.push_back(card_pl2);

        if(m_player2_cards.empty())
            return 1;
    }
    else
    {
        m_player2_cards.push_back(card_pl2);
        m_player2_cards.push_back(card_pl1);

        if(m_player1_cards.empty())
            return 2;
    }

    return 0;
}

uint32_t Day22::play_recursive_combat(std::deque<uint16_t>deck1, std::deque<uint16_t>deck2, uint64_t* score_ptr)
{
    std::set<std::pair<size_t, size_t>> played_rounds;
    uint32_t winner(0);

    while(!winner)
    {
        uint16_t pl1_card(deck1.front()), pl2_card(deck2.front());

        if(!played_rounds.emplace(deque_hash(deck1), deque_hash(deck2)).second)
        {
            winner = 1;
            break;
        }

        deck1.pop_front();
        deck2.pop_front();

        uint32_t round_winner;

        if(pl1_card <= deck1.size() && pl2_card <= deck2.size())
        {
            round_winner = play_recursive_combat({deck1.begin(), deck1.begin() + pl1_card}, {deck2.begin(), deck2.begin() + pl2_card}, nullptr);
        }
        else
        {
            if(pl1_card > pl2_card) round_winner = 1;
            else round_winner = 2;
        }

        if(round_winner == 1)
        {
            deck1.push_back(pl1_card);
            deck1.push_back(pl2_card);

            if(deck2.empty())
                winner = 1;
        }
        else
        {
            deck2.push_back(pl2_card);
            deck2.push_back(pl1_card);

            if(deck1.empty())
                winner = 2;
        }

    }

    if(score_ptr)
    {
        std::deque<uint16_t> * winner_deck;
        if(winner == 1)
            winner_deck = &deck1;
        else
            winner_deck = &deck2;

        size_t mul(1);

        *score_ptr = 0;

        while(!winner_deck->empty())
        {
            *score_ptr += mul * winner_deck->back();
            winner_deck->pop_back();
            mul++;
        }
    }

    return winner;
}

size_t Day22::deque_hash(const std::deque<uint16_t> & input)
{
    size_t hsh(0);

    size_t acc(1);

    for(uint16_t elem : input)
    {
        hsh += elem * acc;
        acc += acc * elem + elem;
    }
    return hsh;
}

