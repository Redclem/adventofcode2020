#include "Day15.h"

Day15::Day15(const char * data)
{
    while(*data)
    {
        char * next;
        m_numbers.push_back(strtoull(data, &next, 10));
        data = next;
        if(*data == ',') data++;
    }
    //ctor
}

Day15::~Day15()
{
    //dtor
}

void Day15::run()
{
    constexpr size_t run_length(30000000);

    size_t * appearances = new size_t[run_length];

    for(size_t i(0); i != run_length;i++)
        appearances[i] = uint64_t(~0);

    for(size_t iter(0); iter != (m_numbers.size() - 1);iter++)
        appearances[m_numbers[iter]] = iter;

    uint64_t last_num;
    uint64_t last_turn;

    m_numbers.reserve(run_length);

    while(m_numbers.size() != run_length)
    {
        last_num = m_numbers.back();
        last_turn = m_numbers.size() - 1;

        auto last_appear = appearances[last_num];

        if(last_appear == uint64_t(~0))
        {
            m_numbers.push_back(0);
        }
        else
        {
            m_numbers.push_back(last_turn - last_appear);
        }

        appearances[last_num] = last_turn;
    }

    delete[] appearances;

    std::cout << "2020th number spoken : " << m_numbers[2019] << '\n';
    std::cout << "30000000th number spoken : " << m_numbers.back() << '\n';
}

