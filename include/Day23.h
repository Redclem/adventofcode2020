#ifndef DAY23_H
#define DAY23_H

#include "Day.h"
#include "Cups.h"

#include <vector>
#include <utility>

class Day23 : public Day
{
public:
    typedef uint32_t cup_t;

    /** Default constructor
     * \param data The input data
     */
    Day23(const char * data);
    /** Default destructor */
    ~Day23();

    void run();

protected:

private:
    Cups m_cups;
};

#endif // DAY23_H
