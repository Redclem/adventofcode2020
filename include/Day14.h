#ifndef DAY14_H
#define DAY14_H

#include "Day.h"

#include <vector>
#include <cstring>
#include <cstdlib>
#include <map>

class Day14 : public Day
{
public:
    /** Default constructor */
    Day14(const char * data);
    /** Default destructor */
    ~Day14();

    void run();

    /** Access memory
     * \param index The index of the memory to address
     * \return A reference to the memory at index
     */
    uint64_t & access_mem(size_t index);

    /** Write with floating bits
     * \param address The base address
     * \param floatbits A number whose set bits represent floating bits in the address
     * \param val The value to write
     */
    void write_float(uint64_t address, uint64_t floatbits, uint64_t val);

protected:

private:
    std::vector<uint64_t> m_memory;
    std::map<uint64_t, uint64_t> m_version2_mem;
};

#endif // DAY14_H
