#ifndef DAY12_H
#define DAY12_H

#include "Day.h"
#include "Ferry.h"

#include <cstdlib>
#include <vector>

class Day12 : public Day
{
public:

    struct Instruction
    {
        char chr;
        Ferry::value_t value;
    };

    /** Default constructor
     * \param data The input data
     */
    Day12(const char * data);
    /** Default destructor */
    ~Day12();
    void run();

protected:

private:
    std::vector<Instruction> m_instructions;
    Ferry m_ferry;
};

#endif // DAY12_H
