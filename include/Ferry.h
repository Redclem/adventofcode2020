#ifndef FERRY_H
#define FERRY_H

#include <stdexcept>
#include <iostream>
#include <cstdlib>

class Ferry
{
public:

    enum class Direction : uint32_t
    {
        NORTH = 0,
        EAST = 1,
        SOUTH = 2,
        WEST = 3
    };

    typedef uint32_t distance_t;
    typedef uint32_t degree_t;
    typedef uint32_t value_t;
    typedef int32_t position_t;

    static constexpr position_t dist_directions[4][2] = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}};

    /** Default constructor */
    Ferry();
    /** Default destructor */
    ~Ferry();

    /** Rotate
     * \param deg The amount of degrees to rotate by
     * \param right True if turning right, false otherwise
     */
    void rotate(degree_t deg, bool right);

    /** Rotate the waypoint
     * \param deg The amount of degrees to rotate by
     * \param right True if turning right, false otherwise
     */
    void rotate_way(degree_t deg, bool right);

    /** Go forward
     * \param dst The amount of distance to travel
     */
    void go_forward(distance_t dst);

    /** Go forward to the waypoint
     * \param dst The amount of times to travel
     */
    void go_forward_way(distance_t dst);

    /** Travel by given distance in given direction
     * \param dst The distance to travel
     * \param dir The direction to travel
     */
    void travel(distance_t dst, Direction dir);

    /** Move the waypoint by given distance in given direction
     * \param dst The distance to move
     * \param dir The direction to move
     */
    void travel_way(distance_t dst, Direction dir);

    /** Get position from char
     * \param chr The input char
     * \return The associated direction
     */
    static Direction dir_from_char(char chr);

    /** Execute the given instruction
     * \param chr The instruction's first char
     * \param value The instruction's value
     */
    void exec_instr(char chr, value_t value);

    /** Execute the given instruction in waypoint mode
     * \param chr The instruction's first char
     * \param value The instruction's value
     */
    void exec_instr_way(char chr, value_t value);

    /** Get manhattan distance from origin
     * \return The manhattan distance from origin
     */
    distance_t manhattan() const {return static_cast<distance_t>(abs(m_x) + abs(m_y));}

    /** Reset the ferrr */
    void reset();

protected:

private:
    Direction m_direction;
    position_t m_x;
    position_t m_y;
    position_t m_way_x;
    position_t m_way_y;
};

#endif // FERRY_H
