#ifndef HANDHELDCONSOLE_H
#define HANDHELDCONSOLE_H

#include <vector>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <stdexcept>

#define IF_OP_MATCH(op) if(strncmp(data, #op, 3) == 0)\
    operation = Operation::op

#define ELSE_IF_OP_MATCH(op) else IF_OP_MATCH(op)

class HandheldConsole
{
public:
    enum class Operation : uint16_t
    {
        nop,
        acc,
        jmp
    };

    typedef int16_t arg_t;

    struct Instruction
    {
        Operation op;
        arg_t argument;
    };

    /** Default constructor */
    HandheldConsole();
    /** Default destructor */
    ~HandheldConsole();
    /** Load instructions from text
     * \param data The input text
     */
    void load_instructions(const char * data);
    /** Step once in the program */
    void step();
    /** Get the accumulator's value
     * \return The accumulator's value
     */
    arg_t get_acc() const {return m_acc;}

    /** Run until an instruction is repeated
     * \return A bool vector representing the executed instructions
     */
    std::vector<bool> run_no_rep();

    /** Subscript operator
     * \param index The index of the instruction to access
     * \return A reference to the instruction at the given index
     */
    Instruction & operator[](size_t index) {return m_instructions[index];}

    /** Check if the program executes the first nonexistent instruction
     * \return true if the program executes the first nonexistent instruction, false otherwise
     */
    bool check_exec_first_nonex();

    /** Reset instruction pointer and accumulator */
    void reset() {m_ip = 0; m_acc = 0;}

protected:

private:
    std::vector<Instruction> m_instructions;
    size_t m_ip;
    arg_t m_acc;
};

#endif // HANDHELDCONSOLE_H
