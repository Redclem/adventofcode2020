#ifndef BAGMANAGER_H
#define BAGMANAGER_H

#include <iostream>
#include <vector>
#include <map>
#include <cstdlib>

class BagManager
{
public:
    struct BagId
    {
        uint16_t first_adj;
        uint16_t second_adj;

        bool operator<(BagId rhs) const
        {
            if(first_adj < rhs.first_adj) return true;
            if(first_adj > rhs.first_adj) return false;
            return second_adj < rhs.second_adj;
        }

        bool operator==(BagId rhs) const
        {
            return first_adj == rhs.first_adj && second_adj == rhs.second_adj;
        }
    };

    struct BagCount
    {
        uint16_t bag_count;
        BagId bag;
    };

    /** Default constructor */
    BagManager() {}
    /** Default destructor */
    ~BagManager() {}

    /** Load bags from rules
     * \param rules The rules as text
     */
    void load_rules(const char * rules);

    /** Get first adj id
     * \param adj The adjective as a string
     * \return The adjective's id
     */
    uint16_t get_first_adj_id(const std::string & adj)
    {
        return m_first_adj_map.insert({adj, m_first_adj_map.size()}).first->second;
    }

    /** Get second adj id
     * \param adj The adjective as a string
     * \return The adjective's id
     */
    uint16_t get_second_adj_id(const std::string & adj)
    {
        return m_second_adj_map.insert({adj, m_second_adj_map.size()}).first->second;
    }

    /** Check if a given bag contains another
     * \param container The container bag id
     * \param contained The bag id to be contained
     * \return true if contained, false otherwise
     */
    bool contains(BagId container, BagId contained) const;

    /** Check how much bag types contain the given bag type
     * \param bag The given bag type
     * \return How much bag types contain the given bag type
     */
    size_t count_contain(BagId bag) const;

    /** Compute how much bags are in a bag
     * \param container The container bag
     * \return The number of bags in the container bag
     */
    size_t count_inside(BagId container) const;

protected:

private:
    std::map<std::string, uint16_t> m_first_adj_map;
    std::map<std::string, uint16_t> m_second_adj_map;

    std::map<BagId, std::vector<BagCount>> m_bags;
};

#endif // BAGMANAGER_H
