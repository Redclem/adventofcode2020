#ifndef DAY10_H
#define DAY10_H

#include "Day.h"

#include <vector>
#include <cstdlib>
#include <algorithm>
#include <cassert>

class Day10 : public Day
{
public:
    typedef uint32_t number_t;

    /** Default constructor
     * \param data The input data
     */
    Day10(const char * data);
    /** Default destructor */
    ~Day10();

    void run();

protected:

private:
    std::vector<number_t> m_adapters;
};

#endif // DAY10_H
