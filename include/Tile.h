#ifndef TILE_H
#define TILE_H

#include <cstddef>
#include <cstdint>

#include <iostream>

class Tile
{
public:
    /** Default constructor */
    Tile();

    /** Construct from data
     * \param data The input data
     */
    Tile(const char * data);

    /** Default destructor */
    ~Tile();

    /** Copy constructor
     * \param from The tile to copy from
     */
    Tile(const Tile & from);

    /** Copy assignement operator
     * \param rhs The tile to copy from
     * \return Itself after assignement
     */
    Tile & operator=(const Tile & rhs);

    /** Load from data
     * \param data The input data
     * \return A pointer to the first character after the tile
     */
    const char * load(const char * data);

    /** Display tile to cout */
    void disp() const;

    /** Change orientation
     * \param amount The orientation change
     */
    void orient(int8_t amount) {m_orientation = (m_orientation + amount) % 8;}

    /** Get tile orientation
     * \return The tile orientation
     */
    uint8_t get_orientation() const {return m_orientation;}

    /** Check if the right of this tile matches the left of another tile
     * \param right The other tile
     * \return true if the right matches the left of the other tile, false otherwise
     */
    bool match_right(const Tile & right);

    /** Check if the bottom of this tile matches the top of another tile
     * \param bottom The other tile
     * \return true if the bottom matches the top of the other tile, false otherwise
     */
    bool match_bottom(const Tile & bottom);

    class ConstTileSub;

    class TileSub
    {
    public:
        friend class ConstTileSub;

        TileSub(char * data, ptrdiff_t stride) : m_data(data), m_stride(stride) {}
        TileSub(const TileSub & from) : m_data(from.m_data), m_stride(from.m_stride) {}
        ~TileSub() {}

        char & operator[](ptrdiff_t index) {return *(m_data + index * m_stride);}
    private:
        char * m_data;
        ptrdiff_t m_stride;
    };

    class ConstTileSub
    {
    public:
        ConstTileSub(const char * data, ptrdiff_t stride) : m_data(data), m_stride(stride) {}
        ConstTileSub(const TileSub & from) : m_data(from.m_data), m_stride(from.m_stride) {}
        ConstTileSub(const ConstTileSub & from) : m_data(from.m_data), m_stride(from.m_stride) {}
        ~ConstTileSub() {}

        const char & operator[](ptrdiff_t index) {return *(m_data + index * m_stride);}
    private:
        const char * m_data;
        ptrdiff_t m_stride;
    };

    /** Subscript operator
     * \param index The index of the row to access
     * \return The row of given index
     */
    TileSub operator[](size_t index);

    /** Subscript operator
     * \param index The index of the row to access
     * \return The row of given index
     */
    ConstTileSub operator[](size_t index) const;

protected:

private:
    char m_data[100];
    uint8_t m_orientation;
};

#endif // TILE_H
