#ifndef DAY2_H
#define DAY2_H

#include "Day.h"

#include <string>
#include <vector>
#include <cstring>
#include <cstdlib>
#include <algorithm>

class Day2 : public Day
{
public:

    struct PasswordAndPolicy
    {
        std::string password;
        uint32_t first_rule_parameter;
        uint32_t second_rule_parameter;
        char criterium_char;
    };

    /** Default constructor
     * \param data The day's data
     */
    Day2(const char * data);
    /** Default destructor */
    ~Day2();
    /** Run the day */
    void run();

protected:

private:
    std::vector<PasswordAndPolicy> m_passwords;
};

#endif // DAY2_H
