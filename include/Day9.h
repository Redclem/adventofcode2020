#ifndef DAY9_H
#define DAY9_H

#include "Day.h"

#include <cstdlib>
#include <vector>

class Day9 : public Day
{
    public:
        typedef uint64_t number_t;

        /** Default constructor
         * \param data The input data
         */
        Day9(const char * data);
        /** Default destructor */
        ~Day9();

        void run();

    protected:

    private:
        std::vector<number_t> m_numbers;
};

#endif // DAY9_H
