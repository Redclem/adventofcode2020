#ifndef DAY17_H
#define DAY17_H

#include "Day.h"

#include <set>
#include <array>
#include <map>

class Day17 : public Day
{
public:
    /** Default constructor */
    Day17(const char * data);
    /** Default destructor */
    ~Day17();

    void run();

    /** Run a cycle */
    void cycle();

    /** Run a cycle in 4 dimensions */
    void cycle4d();

protected:

private:
    std::set<std::array<int64_t, 3>> m_3d_active_cubes;
    std::set<std::array<int64_t, 4>> m_4d_active_cubes;
};

#endif // DAY17_H
