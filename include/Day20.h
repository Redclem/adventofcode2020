#ifndef DAY20_H
#define DAY20_H

#include "Day.h"
#include "Tile.h"
#include "Image.h"

#include <map>
#include <set>
#include <vector>
#include <cmath>
#include <cstring>
#include <cstdlib>
#include <queue>

class Day20 : public Day
{
public:
    /** Default constructor
     * \param data The input data
     */
    Day20(const char * data);
    /** Default destructor */
    ~Day20();

    /** Try assembling the tiles with a given tile as first (in top right corner)
     * \param tiles A reference to a vector containig the tiles on success
     * \param first_tile The first tile's id
     * \param orient The first tile's orientation
     * \return The product of the ids of the corner tiles on success, 0 otherwise
     */
    uint64_t try_tile(std::vector<Tile> & tiles, uint64_t first_tile, uint8_t orient);

    void run();

protected:

private:
    std::map<uint64_t, Tile> m_tiles;
    size_t m_image_side_length;
};

#endif // DAY20_H
