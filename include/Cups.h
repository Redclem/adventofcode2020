#ifndef CUPS_H
#define CUPS_H

#include <vector>
#include <iostream>
#include <cstring>

class Cups
{
    public:
        typedef size_t cup_t;

        /** Default constructor */
        Cups();
        /** Load cups
         * \param data The input data
         */
        void load(const char * data);

        /** Generate cups until given cups
         * \param cup_count how many cups to stop generating at
         */
        void gen_cups(size_t cup_count);

        /** Play one round */
        void round();

        /** Print cups to cout */
        void print_cups();

        /** Print cups after 1 to cout */
        void print_after_1();

        /** Return the product of the two cups after the 1 cup
         * \return The product of the two cups after the 1 cup
         */
        cup_t prod_2();
    protected:

    private:
        std::vector<cup_t> m_cups;
        size_t m_current_cup;
        size_t m_last_cup;
};

#endif // CUPS_H
