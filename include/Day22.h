#ifndef DAY22_H
#define DAY22_H

#include "Day.h"

#include <deque>
#include <iomanip>
#include <set>
#include <utility>
#include <cstring>
#include <cstdlib>
#include <cassert>
#include <functional>

class Day22 : public Day
{
public:
    /** Default constructor
     * \param data The input data
     */
    Day22(const char * data);
    /** Default destructor */
    ~Day22();

    void run();

    /** Play one round of Combat
     * \return The player who won on victory, 0 otherwise
     */
    uint32_t play_round();

    /** Play a game of Recursive Combat
     * \param deck1 Player 1's deck
     * \param deck2 Player 2's deck
     * \param score_ptr A pointer to a variable receiving the score, ignored if nullptr
     * \return The winning player
     */
    uint32_t play_recursive_combat(std::deque<uint16_t> deck1, std::deque<uint16_t> deck2, uint64_t * score_ptr);

    /** Get the hash of a uint16_t deque, used to find decks that already appeared
     * \param input The deque
     * \return The hash
     */
    static size_t deque_hash(const std::deque<uint16_t> & input);

protected:

private:
    std::deque<uint16_t> m_player1_cards;
    std::deque<uint16_t> m_player2_cards;
};

#endif // DAY22_H
