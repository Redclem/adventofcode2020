#ifndef DAY16_H
#define DAY16_H

#include "Day.h"

#include <string>
#include <cstring>
#include <cstdlib>
#include <vector>

class Day16 : public Day
{
public:

    /** A struct representing a field */
    struct Field
    {
        std::string name;
        uint64_t min0;
        uint64_t max0;
        uint64_t min1;
        uint64_t max1;
    };

    /** Default constructor
     * \param data The input data
     */
    Day16(const char * data);
    /** Default destructor */
    ~Day16();

    void run();

    /** Check if a value is valid in any field
     * \param val The value to check
     * \return true if valid, false otherwise
     */
    bool check_value_valid(uint64_t val);

    /** Check if a value is valid in a field
     * \param val The value to check
     * \param fld_id The index of the field to check to value in
     * \return true if valid, false otherwise
     */
    bool check_value_valid_fld(uint64_t val, size_t fld_id)
    {
        Field & fld = m_fields[fld_id];
        return (val >= fld.min0 && val <= fld.max0) || (val >= fld.min1 && val <= fld.max1);
    }

protected:

private:

    std::vector<Field> m_fields;
    std::vector<uint64_t> m_my_ticket;
    std::vector<std::vector<uint64_t>> m_tickets;

};

#endif // DAY16_H
