#ifndef DAY11_H
#define DAY11_H

#include "Day.h"
#include "Map2d.h"

#include <utility>
#include <algorithm>

class Day11 : public Day
{
public:
    /** Default constructor */
    Day11(const char * data);
    /** Default destructor */
    ~Day11();

    /** Simulate people's movement
     * \param input_map The initial layout
     * \return A map with the people's movement simulated
     */
    Map2d simulate(const Map2d & input_map);

    /** Simulate people's movement with non adjacent rules
     * \param input_map The initial layout
     * \return A map with the people's movement simulated
     */
    Map2d simulate_non_adj(const Map2d & input_map);

    /** Simulate one seat's future state
     * \param input_map The initial layout
     * \param col The seat's col
     * \param row The seat's row
     * \return The seat's future state
     */
    uint8_t simulate_seat(const Map2d & input_map, size_t col, size_t row);

    /** Simulate one seat's future state with non adjacent rules
     * \param input_map The initial layout
     * \param col The seat's col
     * \param row The seat's row
     * \return The seat's future state
     */
    uint8_t simulate_seat_non_adj(const Map2d & input_map, size_t col, size_t row);

    void run();

protected:

private:
    Map2d m_seats;
};

#endif // DAY11_H
