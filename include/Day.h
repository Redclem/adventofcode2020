#ifndef DAY_H
#define DAY_H

#include <iostream>

class Day
{
public:
    /** Default constructor */
    Day() {}
    /** Default destructor */
    ~Day() {}
    /** Run the day */
    void run() {}

protected:

private:
};

#endif // DAY_H
