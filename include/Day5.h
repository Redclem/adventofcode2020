#ifndef DAY5_H
#define DAY5_H

#include "Day.h"

#include <map>

class Day5 : public Day
{
public:
    struct Seat
    {
        uint8_t row = 0;
        uint8_t col = 0;
    };

    /** Default constructor
     * \param data The input data
     */
    Day5(const char * data);
    /** Default destructor */
    ~Day5();
    /** Run the day */
    void run();

protected:

private:
    std::map<uint16_t, Seat> m_seats;
};

#endif // DAY5_H
