#ifndef DAY15_H
#define DAY15_H

#include "Day.h"

#include <cstdlib>
#include <vector>
#include <map>

class Day15 : public Day
{
public:

    /** Default constructor
     * \param data The input data
     */
    Day15(const char * data);
    /** Default destructor */
    ~Day15();

    void run();

protected:

private:
    std::vector<uint64_t> m_numbers;
};

#endif // DAY15_H
