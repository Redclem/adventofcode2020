#ifndef IMAGE_H
#define IMAGE_H

#include <vector>
#include <utility>
#include <algorithm>

#include "Tile.h"

constexpr char sea_monster[3][21] = {"                  # ",
                                     "#    ##    ##    ###",
                                     " #  #  #  #  #  #   "
                                    };

constexpr size_t sea_monster_width = 20;
constexpr size_t sea_monster_height = 3;

constexpr size_t sea_monster_cross_count = 15;

class Image
{
public:
    /** Default constructor */
    Image();
    /** Default destructor */
    ~Image();

    /** Load from tile vector
     * \param tiles The tile vector
     * \param side_tiles The image side's length (in tiles)
     */
    void load_tiles(std::vector<Tile> & tiles, size_t side_tiles);

    /** Display image */
    void disp() const;

    /** Try to match sea monster pattern at given pos
     * \param row The row at wich to try matching
     * \param col The col at wich to try matching
     * \return true if pattern matches, false otherwise
     */
    bool try_match(size_t row, size_t col);

    /** Compute the sea monsters' habitat water roughness
     * \return The water roughness
     */
    uint64_t roughness();

    /** Change orientation by given value
     * \param amount The value by wich to change orientation
     */
    void orient(int8_t amount) {m_orient = uint8_t(m_orient + amount) % 8;}

    /** Get value at given coordinates
     * \param row The value's row
     * \param col The value's column
     */
    char & at(size_t row, size_t col);

    /** Get value at given coordinates
     * \param row The value's row
     * \param col The value's column
     */
    const char & at(size_t row, size_t col) const;

protected:

private:
    std::vector<char> m_data;
    size_t m_side_len;
    uint8_t m_orient;
};

#endif // IMAGE_H
