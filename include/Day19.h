#ifndef DAY19_H
#define DAY19_H

#include "Day.h"

#include <cstring>
#include <cctype>
#include <string>
#include <vector>
#include <stack>
#include <map>
#include <cassert>


class Day19 : public Day
{
public:
    /** Default constructor
     * \param data The input data
     */
    Day19(const char * data);
    /** Default destructor */
    ~Day19();

    /** Evaluate if a message matches a rule
     * \param rule The rule number
     * \param msg The message
     * \return true if the message matches the condition, false otherwise
     */
    bool msg_match_rule(size_t rule, const char *& msg);

    /** Evaluate if a message matches rule 0 with modified ruel 8 and 11
     * \param msg The message
     * \return true if the message matches rule 0, false otherwise
     */
    bool msg_match_new_rule(const char *& msg);

    void run();

protected:

private:
    std::map<size_t, std::vector<std::vector<size_t>>> m_composite_rules;
    std::map<size_t, char> m_simple_rules;
    std::vector<std::string> m_messages;
};

#endif // DAY19_H
