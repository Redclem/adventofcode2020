#ifndef DAY1_H
#define DAY1_H

#include <vector>
#include <cstdlib>

#include "Day.h"

class Day1 : public Day
{
public:
    /** Default constructor
     * \param data The day's data
     */
    Day1(const char * data);
    /** Default destructor */
    ~Day1();
    /** Run the day */
    void run();

protected:

private:
    std::vector<uint64_t> m_entries;
};

#endif // DAY1_H
