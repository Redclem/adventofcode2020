#ifndef DAY4_H
#define DAY4_H

#include "Day.h"

#include <cctype>
#include <vector>
#include <cstring>

#define IF_MATCH(field) if(memcmp(#field, field_type, 3) == 0)

#define ELSE_IF_MATCH(field) else IF_MATCH(field)

#define REGISTER_PRESENT(field) cur_passport.present_fields |= Passport::Fields::field

#define REGISTER_VALID(field) cur_passport.valid_fields |= Passport::Fields::field

class Day4 : public Day
{
public:
    struct Passport
    {
        enum class Fields : uint8_t
        {
            byr = 0x01,
            iyr = 0x02,
            eyr = 0x04,
            hgt = 0x08,
            hcl = 0X10,
            ecl = 0X20,
            pid = 0X40
        };

        Fields present_fields;
        Fields valid_fields;
    };

    /** Default constructor
     * \param data The input data
     */
    Day4(const char * data);
    /** Default destructor */
    ~Day4();
    /** Run the day */
    void run();

protected:

private:
    std::vector<Passport> m_passports;
};

#endif // DAY4_H


inline Day4::Passport::Fields & operator|=(Day4::Passport::Fields & lhs, Day4::Passport::Fields rhs)
{
    return reinterpret_cast<Day4::Passport::Fields&>(reinterpret_cast<uint8_t&>(lhs) |= static_cast<uint8_t>(rhs));
}
