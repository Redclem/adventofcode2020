#ifndef DAY13_H
#define DAY13_H

#include "Day.h"
#include "Common_math.h"

#include <vector>
#include <cstdlib>

class Day13 : public Day
{
public:

    /** A struct containing line information */
    struct LineInfo
    {
        uint32_t id;
        uint32_t position;

        /** Default constructor
         * \param _id The line id
         * \param pos The line position
         */
        LineInfo(uint32_t _id, uint32_t pos) : id(_id), position(pos) {}
    };

    /** Default constructor
     * \param data The input data
     */
    Day13(const char * data);
    /** Default destructor */
    ~Day13();

    /** Compute smallest x that satisfies x congruent to 0 mod a and x is congruent to -d mod b
     * \param a The a in the condition above
     * \param b The b in the condition above
     * \param d The d in the condition above
     */
    int64_t solve(int64_t a, int64_t b, int64_t d);

    void run();

protected:

private:
    uint32_t m_earliest_depart;
    std::vector<LineInfo> m_lines;
};

#endif // DAY13_H
