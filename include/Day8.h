#ifndef DAY8_H
#define DAY8_H

#include "Day.h"
#include "HandheldConsole.h"

#include <vector>

class Day8 : public Day
{
public:
    /** Default constructor
     * \param data The input data
     */
    Day8(const char * data);
    /** Default destructor */
    ~Day8();

    void run();

protected:

private:
    HandheldConsole m_console;
};

#endif // DAY8_H
