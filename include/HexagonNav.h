#ifndef HEXAGONNAV_H
#define HEXAGONNAV_H

#include <cstdint>

class HexagonNav
{
    public:

    enum class Direction : uint8_t
    {
        e = 0,
        se,
        sw,
        w,
        nw,
        ne
    };

        /** Default constructor */
        HexagonNav();

        /** Construct with coordinates
         * \param x The x coordinate
         * \param y The y coordinate
         */
        HexagonNav(int64_t x, int64_t y) : m_x(x), m_y(y) {}

        /** Get x coordinate
         * \return The x coordinate
         */
        int64_t x() const {return m_x;}

        /** Get y coordinate
         * \return The y coordinate
         */
        int64_t y() const {return m_y;}

        /** Return to coordinates 0, 0 */
        void to_origin() {m_x = 0; m_y = 0;}

        /** Move according to a direction
         * \param dir The direction to move according to
         */
        void move_dir(Direction dir);

        /** Equality operator
         * \param rhs The object to compare with
         * \return true if equal, false otherwise
         */
        bool operator==(const HexagonNav & rhs) const {return m_x == rhs.m_x && m_y == rhs.m_y;}

        /** Inequality operator
         * \param rhs The object to compare with
         * \return true if inequal, false otherwise
         */
        bool operator!=(const HexagonNav & rhs) const {return m_x != rhs.m_x || m_y != rhs.m_y;}

        /** Less than operator
         * \param rhs The object to compare with
         * \return true if less than rhs, false otherwise
         */
        bool operator<(const HexagonNav & rhs) const {return (m_x == rhs.m_x ? m_y < rhs.m_y : m_x < rhs.m_x);}

        /** Greater than operator
         * \param rhs The object to compare with
         * \return true if greater than rhs, false otherwise
         */
        bool operator>(const HexagonNav & rhs) const {return (m_x == rhs.m_x ? m_y > rhs.m_y : m_x > rhs.m_x);}

    protected:

    private:
        int64_t m_x;
        int64_t m_y;
};

#endif // HEXAGONNAV_H
