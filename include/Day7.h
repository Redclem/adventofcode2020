#ifndef DAY7_H
#define DAY7_H

#include "BagManager.h"

class Day7
{
public:
    /** Default constructor
     * \param data The input data
     */
    Day7(const char * data);
    /** Default destructor */
    ~Day7();

    void run();

protected:

private:
    BagManager m_bag_manager;
};

#endif // DAY7_H
