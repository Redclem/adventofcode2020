#ifndef MAP2D_H
#define MAP2D_H

#include <vector>
#include <utility>
#include <iostream>

class Map2d
{
public:
    /** Default constructor */
    Map2d();

    /** Construct and fill
     * \param cols The column count
     * \param rows The row count
     * \param init_state The initial state of each position
     */
    Map2d(size_t cols, size_t rows, uint8_t init_state);



    /** Construct from data
     * \param data The data to construct from
     */
    Map2d(const char * data);
    /** Read data
     * \param data The data to read
     */
    void read(const char * data);
    /** Default destructor */
    ~Map2d();

    /** Move constructor
    * \param from The object to move
    */
    Map2d(Map2d && from);

    /** Move assignement operator
    * \param rhs The object to move
    * \return Itself after assignement
    */
    Map2d & operator=(Map2d && rhs);

    /** Copy constructor
    * \param from The object to copy
    */
    Map2d(const Map2d & from);

    /** Copy assignement operator
    * \param rhs The object to copy
    * \return Itself after assignement
    */
    Map2d & operator=(const Map2d & rhs);

    /** Get row length
     * \return The length of a row
     */
    size_t get_row_len() const
    {
        return m_row_len;
    }

    /** Get col length
     * \return The length of a column
     */
    size_t get_col_len() const
    {
        return m_data.size() / m_row_len;
    }

    /** Get row count (= col length)
     * \return the row count
     */
    size_t get_row_count() const
    {
        return m_data.size() / m_row_len;
    }

    /** Get col count (= row length)
     * \return The col count
     */
    size_t get_col_count() const
    {
        return m_row_len;
    }

    class Row
    {
    private:
        size_t m_row_len;
        uint8_t * m_data;
    public:
        /** Index this row
         * \param index The index of the element to get
         * \return The element at the given index in this row
         */
        uint8_t & operator[](size_t index)
        {
            return m_data[index % m_row_len];
        }

        /** Index this row
         * \param index The index of the element to get
         * \return The element at the given index in this row
         */
        const uint8_t & operator[](size_t index) const
        {
            return m_data[index % m_row_len];
        }

        /** Convert to uint8_t*
         * \return This row as a uint8_t *
         */
        operator uint8_t* ()
        {
            return m_data;
        }

        /** Convert to const uint8_t*
         * \return This row as a const uint8_t *
         */
        operator const uint8_t* () const
        {
            return m_data;
        }
    private:
        Row(size_t row_len, uint8_t * data) : m_row_len(row_len), m_data(data) {}

        friend class Map2d;
    };

    class const_Row
    {
    private:
        size_t m_row_len;
        const uint8_t * m_data;
    public:
        /** Index this row
         * \param index The index of the element to get
         * \return The element at the given index in this row
         */
        const uint8_t & operator[](size_t index) const
        {
            return m_data[index % m_row_len];
        }

        /** Convert to const uint8_t*
         * \return This row as a const uint8_t *
         */
        operator const uint8_t* () const
        {
            return m_data;
        }
    private:

        const_Row(size_t row_len, const uint8_t * data) : m_row_len(row_len), m_data(data) {}

        friend class Map2d;
    };

    /** Index this map (row first, column second)
     * \param index The index of the row to access
     * \return The row of the given index
     */
    const_Row operator[](size_t index) const
    {
        return const_Row(m_row_len, m_data.data() + m_row_len * index);
    }

    /** Index this map (row first, column second)
     * \param index The index of the row to access
     * \return The row of the given index
     */
    Row operator[](size_t index)
    {
        return Row(m_row_len, m_data.data() + m_row_len * index);
    }

    /** Display the map to cout */
    void disp();

    /** Equality
     * \param rhs The map to compare
     * \return true if this is equal to rhs, false otherwise
     */
    bool operator==(const Map2d & rhs) const {return m_row_len == rhs.m_row_len && m_data == rhs.m_data;}

    /** Inequality
     * \param rhs The map to compare
     * \return true if this is inequal to rhs, false otherwise
     */
    bool operator!=(const Map2d & rhs) const {return m_row_len != rhs.m_row_len || m_data != rhs.m_data;}

    /** Get a reference to the data
     * \return A reference to the data
     */
    std::vector<uint8_t> & get_data()
    {
        return m_data;
    }

    /** Get a const reference to the data
     * \return A const reference to the data
     */
    const std::vector<uint8_t> & get_data() const
    {
        return m_data;
    }

protected:

private:
    std::vector<uint8_t> m_data;
    size_t m_row_len;
};

#endif // Map2d_H
