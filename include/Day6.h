#ifndef DAY6_H
#define DAY6_H

#include "Day.h"

#include <vector>

class Day6 : public Day
{
public:

    /** A struct containing the answers to a form */
    struct Form
    {
        /** An enumeration representing the state of the answer */
        enum class Answer : uint8_t
        {
            ANSWER_NONE = 0, /** Nobody answered the question positively */
            ANSWER_EVERYONE = 0x01, /** So far everyone answered the question positively, or no one was checked yet */
            ANSWER_SOMEONE = 0x02 /** Someone answered the question positively */
        };

        Answer answers[26]; /** The 26 answers in the form (one for each letter) */

        /** Default constructor : nobody was checked, so set all to Answer::ANSWER_EVERYONE */
        Form()
        {
            for(uint8_t ans(0); ans != 26; ans++) answers[ans] = Answer::ANSWER_EVERYONE;
        }

        /** Access an answer by its index
         * \param index The index of the answer
         * \return A reference to the answer
         */
        Answer & operator[](uint8_t index)
        {
            return answers[index];
        }

        /** Access an answer by its index
         * \param index The index of the answer
         * \return A const reference to the answer
         */
        const Answer & operator[](uint8_t index) const
        {
            return answers[index];
        }
    };

    /** Default constructor
     * \param data The input data
     */
    Day6(const char * data);
    /** Default destructor */
    ~Day6();
    /** Run the day */
    void run();

protected:

private:
    std::vector<Form> m_forms;
};

inline Day6::Form::Answer & operator&=(Day6::Form::Answer & lhs, Day6::Form::Answer rhs)
{
    return reinterpret_cast<Day6::Form::Answer&>(reinterpret_cast<uint8_t&>(lhs) &= static_cast<uint8_t>(rhs));
}

inline Day6::Form::Answer & operator|=(Day6::Form::Answer & lhs, Day6::Form::Answer rhs)
{
    return reinterpret_cast<Day6::Form::Answer&>(reinterpret_cast<uint8_t&>(lhs) |= static_cast<uint8_t>(rhs));
}

inline Day6::Form::Answer operator&(Day6::Form::Answer lhs, Day6::Form::Answer rhs)
{
    return static_cast<Day6::Form::Answer>(static_cast<uint8_t>(lhs) & static_cast<uint8_t>(rhs));
}

#endif // DAY6_H

