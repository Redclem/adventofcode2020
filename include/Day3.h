#ifndef DAY3_H
#define DAY3_H

#include "Day.h"
#include "Map2d.h"


class Day3 : public Day
{
public:
    /** Default constructor
     * \param data The day's data
     */
    Day3(const char * data);
    /** Default destructor */
    ~Day3();
    /** Run the day */
    void run();

protected:

private:
    Map2d m_map;
};

#endif // DAY3_H
