#ifndef HEXAGONTILING_H
#define HEXAGONTILING_H

#include "HexagonNav.h"

#include <set>
#include <map>
#include <utility>
#include <cassert>
#include <iostream>

class HexagonTiling
{
    public:
        /** Default constructor */
        HexagonTiling() {}

        /** Flip a tile
         * \param tile The tile to flip
         */
        void flip(const HexagonNav & tile);

        /** Get the number of flipped tiles
         * \return How many tiles are flipped
         */
        size_t count_flipped() const {return m_flipped_tiles.size();}

        /** Flip all tiles for the art exhibit */
        void flip_art();

    protected:

    private:
        std::set<HexagonNav> m_flipped_tiles;
};

#endif // HEXAGONTILING_H
