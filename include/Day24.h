#ifndef DAY24_H
#define DAY24_H

#include "Day.h"
#include "HexagonNav.h"
#include "HexagonTiling.h"

#include <vector>
#include <set>
#include <cstdint>

class Day24 : public Day
{
public:

    /** Default constructor
     * \param data The input data
     */
    Day24(const char * data);
    /** Default destructor */
    ~Day24();

    void run();

protected:

private:
    std::vector<std::vector<HexagonNav::Direction>> m_directions;
};

#endif // DAY24_H
