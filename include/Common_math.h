#ifndef MATH_H_INCLUDED
#define MATH_H_INCLUDED

#include <type_traits>
#include <iostream>
#include <cassert>

namespace Math
{
    /** Modular inverse of a mod b (return m such that (a * m) % b == 1)
     * \tparam T the type of the parameters and return type (Must be signed !)
     * \param a The number whose inverse will be computed
     * \param b The modulo
     * \return The modular inverse of a mod b
     */
    template<typename T>
    T modular_inverse(T a, T b)
    {
        T orig_b = b;

        struct Decomp
        {
            T fac_a;
            T fac_b;
        } dec_a = {1, 0}, dec_b = {0, 1};

        while(a % b)
        {
            T tmp = b;
            T quot = a / b;
            b = a % b;
            a = tmp;

            Decomp dec_tmp = dec_b;

            dec_b = {dec_a.fac_a - (quot * dec_b.fac_a) , dec_a.fac_b - (quot * dec_b.fac_b)};

            dec_a = dec_tmp;
        }

        assert(b == 1);

        return (dec_b.fac_a < 0 ? dec_b.fac_a + orig_b : dec_b.fac_a);
    }
}

#endif // MATH_H_INCLUDED
