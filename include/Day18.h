#ifndef DAY18_H
#define DAY18_H

#include "Day.h"

#include <vector>
#include <string>
#include <cctype>


class Day18 : public Day
{
public:
    /** Default constructor
     * \param data The input data
     */
    Day18(const char * data);
    /** Default destructor */
    ~Day18();

    void run();

    /** Evaluate an expression
     * \param txt The expression in text format
     * \return THe value of the expression
     */
    static uint64_t evaluate_expression(const char * & txt);

    /** Evaluate a value
     * \param txt The value in text format
     * \return THe value
     */
    static uint64_t evaluate_val(const char * & txt);

    /** Evaluate an expression w/ addition priority
     * \param txt The expression in text format
     * \return THe value of the expression
     */
    static uint64_t evaluate_expression_prio(const char * & txt);

    /** Evaluate a value w/ addition priority
     * \param txt The value in text format
     * \return THe value
     */
    static uint64_t evaluate_val_prio(const char * & txt);

protected:

private:
    std::vector<std::string> m_expressions;
};

#endif // DAY18_H
