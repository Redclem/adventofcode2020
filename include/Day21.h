#ifndef DAY21_H
#define DAY21_H

#include "Day.h"

#include <string>
#include <map>
#include <set>
#include <vector>
#include <cctype>
#include <cstring>

class Day21 : public Day
{
public:
    struct IngredientList
    {
        std::set<uint64_t> ingredients;
        std::set<uint64_t> allergens;
    };

    /** Default constructor
     * \param data The input data
     */

    Day21(const char * data);

    /** Default destructor */
    ~Day21();

    void run();

    /** Is the number a power of 2
     * \param number The number to check
     * \param power If not nullptr and if the call returns true, the integer n so that 2^n == number
     * \return true if number if a power of two, false otherwise
     */
    static bool is_power_of_2(uint64_t number, uint64_t * power = nullptr);

protected:

private:
    std::map<std::string, uint64_t> m_ingredient_map;
    std::map<std::string, uint64_t> m_allergen_map;
    std::vector<IngredientList> m_ingredient_lists;
};

#endif // DAY21_H
